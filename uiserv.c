/*
	The BananUI server. Handles commands from apps.
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "ui.h"
#define MAX_SOCK_FDS 128
#define MAX_WAITING_CLIENTS 5
#define MAX_COMMAND_LENGTH 1024
#define FLASHLIGHT_FILE "/sys/class/leds/flashlight/brightness"
#define VIBRATOR_FILE "/sys/class/timed_output/vibrator/enable"

enum bintype { BINARY_NONE = 0, BINARY_IMAGE = 1 };
enum style_type {
	STYLE_TYPE_WINDOW_WIDGET,
	STYLE_TYPE_SOFTKEY,
	STYLE_TYPE_TOPPANEL,
	STYLE_TYPE_FOCUS
};

struct current_style {
	int fgr, fgg, fgb;
	int bgr, bgg, bgb;
	int focr, focg, focb;
	int borr, borg, borb;
};

struct mydata {
	struct uiinfo *uiinf;
	struct ui_window *win;
	struct ui_window* windows[MAX_SOCK_FDS];
	struct current_style* styles[MAX_SOCK_FDS];
	int sockfds[MAX_SOCK_FDS], nsockfds;
	int bufindex[MAX_SOCK_FDS];
	enum bintype isbinary[MAX_SOCK_FDS];
	char buffers[MAX_SOCK_FDS][MAX_COMMAND_LENGTH];
	struct ui_widget* cbx[MAX_SOCK_FDS];
	struct ui_widget* img[MAX_SOCK_FDS];
} mydat /* Global variable */;

static void initStyle(struct current_style *style){
	style->fgr = style->fgg = style->fgb = 0;
	style->bgr = style->bgg = style->bgb = 0xA0;
	style->focr = style->focg = style->focb = 0x40;
	style->borr = style->borg = 0xA0;
	style->borb = 0xF0;
}

static void initWidgetStyle(struct mydata *mydat, struct ui_style *style,
	int i)
{
	struct current_style *cst = mydat->styles[i];
	style->fgcolor = from_rgb(cst->fgr, cst->fgg, cst->fgb);
	style->bgcolor = from_rgb(cst->bgr, cst->bgg, cst->bgb);
	style->focolor = from_rgb(cst->focr, cst->focg, cst->focb);
	style->bocolor = from_rgb(cst->borr, cst->borg, cst->borb);
}

static int doLabel(struct mydata *mydat, struct ui_window *win,
	struct ui_widget *widget, const char *command)
{
	widget->type = UI_LABEL;
	switch(command[2]){
		case 'l':
			widget->textalign = UI_TA_LEFT;
			break;
		case 'c':
			widget->textalign = UI_TA_CENTER;
			break;
		case 'r':
			widget->textalign = UI_TA_RIGHT;
			break;
		default:
			return 0;
	}
	if(command[3] != ' ') return 0;
	widget->data = malloc(strlen(command+3));
	widget->freedata = 1;
	strcpy((char*) widget->data, command+4);
	widget->rightend = getxres(mydat->uiinf->fbinf);
	return 1;
}
static int doIcon(struct mydata *mydat, struct ui_window *win,
	struct ui_widget *widget, const char *command)
{
	struct icon_widget_data *data;
	char *end;
	widget->type = UI_ICON;
	switch(command[2]){
		case 'l':
			widget->textalign = UI_TA_LEFT;
			break;
		case 'c':
			widget->textalign = UI_TA_CENTER;
			break;
		case 'r':
			widget->textalign = UI_TA_RIGHT;
			break;
		default:
			return 0;
	}
	if(command[3] != ' ') return 0;
	widget->data = data = malloc(sizeof(struct icon_widget_data));
	widget->freedata = 1;
	data->width = strtol(command+4, &end, 10);
	if(*end != ' '){
		free(widget->data);
		return 0;
	}
	data->height = strtol(end+1, &end, 10);
	if(*end != ' '){
		free(widget->data);
		return 0;
	}
	strncpy(data->name, end+1, 255);
	data->name[255] = '\0';
	widget->rightend = getxres(mydat->uiinf->fbinf);
	return 1;
}
static void saveByte(struct mydata *mydat, int clientid, char byte)
{
	unsigned char *data;
	data = mydat->img[clientid]->data;
	if(!data){
		data = malloc(2);
		data[0] = byte;
		data[1] = 0;
		mydat->bufindex[clientid] = 0;
	}
	else if(!data[1]){
		data = realloc(data, byte * data[0] * 4 + 2);
		mydat->img[clientid]->freedata = 1;
		data[1] = byte;
	}
	else {
		data[2 + mydat->bufindex[clientid]] = byte;
		mydat->bufindex[clientid]++;
		if(mydat->bufindex[clientid] == data[0] * data[1] * 4)
		{
			if(mydat->cbx[clientid])
				addWidgetToCbx(mydat->uiinf,
					mydat->windows[clientid],
					mydat->cbx[clientid],
					mydat->img[clientid], 0);
			else
				addWidget(mydat->uiinf,
					mydat->windows[clientid],
					mydat->img[clientid], 0);
			mydat->isbinary[clientid] = 0;
			mydat->img[clientid] = NULL;
			mydat->bufindex[clientid] = 0;
			return;
		}
	}
	mydat->img[clientid]->data = data;
}
static int doImage(struct mydata *mydat, struct ui_window *win,
	struct ui_widget *widget, const char *command, int clientid)
{
	widget->type = UI_IMAGE;
	switch(command[2]){
		case 'l':
			widget->textalign = UI_TA_LEFT;
			break;
		case 'c':
			widget->textalign = UI_TA_CENTER;
			break;
		case 'r':
			widget->textalign = UI_TA_RIGHT;
			break;
		default:
			return 0;
	}
	if(command[3] != ' ') return 0;
	widget->data = NULL;
	widget->freedata = 0;
	mydat->isbinary[clientid] = BINARY_IMAGE;
	mydat->img[clientid] = widget;
	widget->rightend = getxres(mydat->uiinf->fbinf);
	return 1;
}
static int doButton(struct mydata *mydat, struct ui_window *win,
	struct ui_widget *widget, const char *command)
{
	widget->type = UI_BUTTON;
	widget->data = malloc(strlen(command+3));
	widget->freedata = 1;
	strcpy((char*) widget->data, command+4);
	widget->rightend = getxres(mydat->uiinf->fbinf);
	return 1;
}

static int doInput(struct mydata *mydat, struct ui_window *win,
	struct ui_widget *widget, const char *command)
{
	widget->type = UI_INPUT;
	widget->data = malloc(258);
	widget->freedata = 1;
	switch(command[4]){
		case 'p':
			widget->inputtype = UI_INP_PASSWORD;
			break;
		default:
			widget->inputtype = UI_INP_NORMAL;
	}
	if(command[4] && command[5] == ' '){
		strncpy(((char*)widget->data) + 2, command+6, 256);
	}
	else {
		((char*)widget->data)[2] = 0;
	}
	((char*)widget->data)[0] = 0;
	((char*)widget->data)[1] = 0;
	widget->rightend = getxres(mydat->uiinf->fbinf);
	return 1;
}
static void returnContents(struct mydata *mydat, int fd,
	struct ui_widget *widget)
{
	write(fd, "set ", 4); /* So it won't conflict with events */
	switch(widget->type){
		case UI_IMAGE:
		case UI_LABEL:
		case UI_BUTTON:
		case UI_FOBUTTON:
			write(fd, widget->data,
				strlen((char*)widget->data));
			break;
		case UI_INPUT:
		case UI_FOINPUT:
			write(fd, ((char*)widget->data)+2,
				strlen(((char*)widget->data)+2));
			break;
		case UI_CBX:
			write(fd, "CBX", 3);
			break;
		case UI_ICON:
			write(fd, ((struct icon_widget_data*)widget->data)->
				name, strlen(
					(
						(struct icon_widget_data*)
						widget->data
					)->name));
			break;
	}
}
static void handleStyle(const char *command,
	struct current_style *sty, struct ui_window *win,
	enum style_type type)
{
	char *end;
	if(type == STYLE_TYPE_SOFTKEY){
		if(0 == strncmp(command, "background ", 11)){
			win->skbgr = strtol(command+11, &end, 10);
			if(*end != ' ') return;
			win->skbgg = strtol(end+1, &end, 10);
			if(*end != ' ') return;
			win->skbgb = strtol(end+1, &end, 10);
		}
		else if(0 == strncmp(command, "color ", 6)){
			win->skfgr = strtol(command+6, &end, 10);
			if(*end != ' ') return;
			win->skfgg = strtol(end+1, &end, 10);
			if(*end != ' ') return;
			win->skfgb = strtol(end+1, &end, 10);
		}
	}
	else if(type == STYLE_TYPE_TOPPANEL){
		if(0 == strncmp(command, "background ", 11)){
			win->tpbgr = strtol(command+11, &end, 10);
			if(*end != ' ') return;
			win->tpbgg = strtol(end+1, &end, 10);
			if(*end != ' ') return;
			win->tpbgb = strtol(end+1, &end, 10);
		}
		else if(0 == strncmp(command, "color ", 6)){
			win->tpfgr = strtol(command+6, &end, 10);
			if(*end != ' ') return;
			win->tpfgg = strtol(end+1, &end, 10);
			if(*end != ' ') return;
			win->tpfgb = strtol(end+1, &end, 10);
		}
	}
	else if(type == STYLE_TYPE_WINDOW_WIDGET){
		if(0 == strncmp(command, "background ", 11)){
			sty->bgr = strtol(command+11, &end, 10);
			win->bgr = sty->bgr;
			if(*end != ' ') return;
			sty->bgg = strtol(end+1, &end, 10);
			win->bgg = sty->bgg;
			if(*end != ' ') return;
			sty->bgb = strtol(end+1, &end, 10);
			win->bgb = sty->bgb;
		}
		else if(0 == strncmp(command, "color ", 6)){
			sty->fgr = strtol(command+6, &end, 10);
			if(*end != ' ') return;
			sty->fgg = strtol(end+1, &end, 10);
			if(*end != ' ') return;
			sty->fgb = strtol(end+1, &end, 10);
		}
		else if(0 == strncmp(command, "border ", 7)){
			sty->borr = strtol(command+7, &end, 10);
			if(*end != ' ') return;
			sty->borg = strtol(end+1, &end, 10);
			if(*end != ' ') return;
			sty->borb = strtol(end+1, &end, 10);
		}
	}
	else if(type == STYLE_TYPE_FOCUS){
		if(0 == strncmp(command, "border ", 7)){
			sty->focr = strtol(command+7, &end, 10);
			if(*end != ' ') return;
			sty->focg = strtol(end+1, &end, 10);
			if(*end != ' ') return;
			sty->focb = strtol(end+1, &end, 10);
		}
	}
}
static void serverCommand(struct mydata *mydat, int i, const char *command)
{
	if(0 == strncmp(command, "btn ", 4)){
		struct ui_widget *widget;
		widget = malloc(sizeof(struct ui_widget));
		initWidgetStyle(mydat, &widget->style, i);
		if(!doButton(mydat, mydat->windows[i], widget, command)){
			free(widget);
			return;
		}
		if(mydat->cbx[i]){
			addWidgetToCbx(mydat->uiinf, mydat->windows[i],
				mydat->cbx[i], widget, 1);
			mydat->cbx[i]->focusable = 1;
		}
		else
			addWidget(mydat->uiinf, mydat->windows[i], widget,
				1);
	}
	else if(0 == strncmp(command, "inp ", 4)){
		struct ui_widget *widget;
		widget = malloc(sizeof(struct ui_widget));
		initWidgetStyle(mydat, &widget->style, i);
		if(!doInput(mydat, mydat->windows[i], widget, command)){
			free(widget);
			return;
		}
		if(mydat->cbx[i]){
			addWidgetToCbx(mydat->uiinf, mydat->windows[i],
				mydat->cbx[i], widget, 1);
			mydat->cbx[i]->focusable = 1;
		}
		else
			addWidget(mydat->uiinf, mydat->windows[i],
				widget, 1);
	}
	else if(0 == strncmp(command, "lb", 2)){
		struct ui_widget *widget;
		widget = malloc(sizeof(struct ui_widget));
		initWidgetStyle(mydat, &widget->style, i);
		if(!doLabel(mydat, mydat->windows[i], widget, command)){
			free(widget);
			return;
		}
		if(mydat->cbx[i])
			addWidgetToCbx(mydat->uiinf, mydat->windows[i],
				mydat->cbx[i], widget, 0);
		else
		addWidget(mydat->uiinf, mydat->windows[i],
			widget, 0);
	}
	else if(0 == strncmp(command, "ic", 2)){
		struct ui_widget *widget;
		widget = malloc(sizeof(struct ui_widget));
		initWidgetStyle(mydat, &widget->style, i);
		if(!doIcon(mydat, mydat->windows[i], widget, command)){
			free(widget);
			return;
		}
		if(mydat->cbx[i])
			addWidgetToCbx(mydat->uiinf, mydat->windows[i],
				mydat->cbx[i], widget, 0);
		else
		addWidget(mydat->uiinf, mydat->windows[i],
			widget, 0);
	}
	else if(0 == strncmp(command, "im", 2)){
		struct ui_widget *widget;
		widget = malloc(sizeof(struct ui_widget));
		initWidgetStyle(mydat, &widget->style, i);
		if(!doImage(mydat, mydat->windows[i], widget, command, i)){
			free(widget);
			return;
		}
	}
	else if(0 == strncmp(command, "cbx ", 4)){
		struct ui_cbx_info *cbxi;
		if(mydat->cbx[i]) return;
		mydat->cbx[i] = malloc(sizeof(struct ui_widget));
		mydat->cbx[i]->type = UI_CBX;
		cbxi = malloc(sizeof(struct ui_cbx_info));
		cbxi->firstfoc = cbxi->lastfoc = cbxi->first = cbxi->last =
			NULL;
		cbxi->numwidgets = 0;
		mydat->cbx[i]->data = cbxi;
		mydat->cbx[i]->freedata = 1;
		mydat->cbx[i]->focusable = 0;
	}
	else if(0 == strncmp(command, "cbe ", 4)){
		if(!mydat->cbx[i]) return;
		initWidgetStyle(mydat, &mydat->cbx[i]->style, i);
		addWidget(mydat->uiinf, mydat->windows[i],
			mydat->cbx[i], mydat->cbx[i]->focusable);
		mydat->cbx[i] = NULL;
	}
	else if(0 == strncmp(command, "rep ", 4)){
		char *other;
		int id;
		struct ui_widget *widget;
		id = strtol(command+4, &other, 10);
		if(id >= MAX_WIDGETS || id < 0 ||
			!mydat->windows[i]->widgets[id]) return;
		widget = mydat->windows[i]->widgets[id];
		if(0 == strncmp(other, " btn ", 5)){
			initWidgetStyle(mydat, &widget->style, i);
			if(!doButton(mydat, mydat->windows[i], widget,
				other+1))
				return;
			if(!widget->focusable){
				makeFocusable(mydat->uiinf, mydat->windows[i],
					widget);
			}
		}
		if(0 == strncmp(other, " inp ", 5)){
			initWidgetStyle(mydat, &widget->style, i);
			if(!doInput(mydat, mydat->windows[i], widget,
				other+1))
				return;
			if(!widget->focusable){
				makeFocusable(mydat->uiinf, mydat->windows[i],
					widget);
			}
		}
		else if(0 == strncmp(other, " lb", 3)){
			initWidgetStyle(mydat, &widget->style, i);
			if(!doLabel(mydat, mydat->windows[i], widget,
				other+1))
				return;
			if(widget->focusable){
				makeUnfocusable(mydat->uiinf, mydat->windows[i],
					widget);
			}
		}
		else if(0 == strncmp(other, " ic", 3)){
			initWidgetStyle(mydat, &widget->style, i);
			if(!doIcon(mydat, mydat->windows[i], widget, other+1)){
				free(widget);
				return;
			}
			if(widget->focusable){
				makeUnfocusable(mydat->uiinf, mydat->windows[i],
					widget);
			}
		}
		else if(0 == strncmp(other, " im", 3)){
			initWidgetStyle(mydat, &widget->style, i);
			if(!doImage(mydat, mydat->windows[i], widget,
				other+1, i))
				return;
			if(widget->focusable){
				makeUnfocusable(mydat->uiinf, mydat->windows[i],
					widget);
			}
		}
	}
	else if(0 == strncmp(command, "rfr ", 4)){
		if(command[4] == 'c'){
			redrawWindowTransition(mydat->uiinf, mydat->windows[i],
				TRANS_CROSS, 200000);
		}
		else {
			redrawWindow(mydat->uiinf, mydat->windows[i]);
		}
	}
	else if(0 == strncmp(command, "get ", 4)){
		int id;
		id = strtol(command+4, NULL, 10);
		if(id < MAX_WIDGETS && id >= 0 &&
				mydat->windows[i]->widgets[id])
		{
			returnContents(mydat, mydat->sockfds[i],
				mydat->windows[i]->widgets[id]);
		}
		write(mydat->sockfds[i], "\n", 1);
	}
	else if(0 == strncmp(command, "tit ", 4)){
		strncpy(mydat->windows[i]->title, command+4, TITLE_SIZE-1);
		mydat->windows[i]->title[TITLE_SIZE-1] = '\0';
	}
	else if(0 == strncmp(command, "wak ", 4)){
		handlePowerOn(mydat->uiinf);
	}
	else if(0 == strncmp(command, "sk", 2)){
		char dummy[SK_LABEL_SIZE], *dest;
		if(!command[2] || command[3] != ' ') return;
		dest = (command[2] == 'l' ? mydat->windows[i]->current_skl
			: command[2] == 'c' ? mydat->windows[i]->current_skc
			: command[2] == 'r' ? mydat->windows[i]->current_skr
			: dummy);
		strncpy(dest, command+4, SK_LABEL_SIZE-1);
		dest[SK_LABEL_SIZE-1] = '\0';
	}
	else if(0 == strncmp(command, "clr ", 4)){
		clearWindow(mydat->uiinf, mydat->windows[i]);
	}
	else if(0 == strncmp(command, "ict ", 4)){
		strncpy(mydat->windows[i]->icontheme, command+4,
			ICON_THEME_NAME_SIZE-1);
		mydat->windows[i]->icontheme[ICON_THEME_NAME_SIZE-1] = '\0';
	}
	else if(0 == strncmp(command, "fla ", 4)){
		int stat, ledfd;
		ledfd = open(FLASHLIGHT_FILE, O_WRONLY);
		stat = strtol(command+4, NULL, 10);
		if(stat){
			write(ledfd, "1\n", 2);
		}
		else {
			write(ledfd, "0\n", 2);
		}
		close(ledfd);
	}
	else if(0 == strncmp(command, "vib ", 4)){
		int vibfd;
		vibfd = open(VIBRATOR_FILE, O_WRONLY);
		write(vibfd, command+4, strlen(command+4));
		write(vibfd, "\n", 1);
		close(vibfd);
	}
	else if(0 == strncmp(command, "htp ", 4)){
		mydat->windows[i]->hidetoppanel =
			!mydat->windows[i]->hidetoppanel;
	}
	else if(0 == strncmp(command, "hsp ", 4)){
		mydat->windows[i]->hideskpanel =
			!mydat->windows[i]->hideskpanel;
	}
	else if(0 == strncmp(command, "sty ", 4)){
		if(!command[4] || command[5] != ' ') return;
		switch(command[4]){
			case 's':
				handleStyle(command+6, mydat->styles[i],
					mydat->windows[i], STYLE_TYPE_SOFTKEY);
				break;
			case 'f':
				handleStyle(command+6, mydat->styles[i],
					mydat->windows[i], STYLE_TYPE_FOCUS);
				break;
			case 't':
				handleStyle(command+6, mydat->styles[i],
					mydat->windows[i], STYLE_TYPE_TOPPANEL);
				break;
			default:
				handleStyle(command+6, mydat->styles[i],
					mydat->windows[i],
					STYLE_TYPE_WINDOW_WIDGET);
		}
	}
}

static void clientEventCallback(void *data, int id, enum ui_event ev)
{
	char outbuf[15];
	char *code;
	int fd;
	fd = *((int*)data);
	switch(ev){
		case UI_EVENT_CLICK:
			code = "clk";
			break;
		case UI_EVENT_KEYDOWN:
			code = "kdn";
			break;
		case UI_EVENT_KEYUP:
			code = "kup";
			break;
		case UI_EVENT_EXIT:
			code = "exi";
			break;
		case UI_EVENT_FOCUS:
			code = "foc";
			break;
		default:
			return;
	}
	snprintf(outbuf, 15, "%s %d\n", code, id);
	write(fd, outbuf, strlen(outbuf));
}
static void fdCallback(void *data, int i)
{
	struct mydata *mydat = data;
	if(i == 0){
		if(mydat->nsockfds == MAX_SOCK_FDS){
			fprintf(stderr, "Too many clients\n");
			return;
		}
		mydat->sockfds[mydat->nsockfds] =
			accept(mydat->sockfds[i], NULL, NULL);
		fcntl(mydat->sockfds[mydat->nsockfds], F_SETFD, FD_CLOEXEC);
		printf("Client! Id: %d\n", mydat->nsockfds);
		mydat->bufindex[mydat->nsockfds] = 0;
		mydat->isbinary[mydat->nsockfds] = BINARY_NONE;
		mydat->cbx[mydat->nsockfds] = NULL;
		mydat->img[mydat->nsockfds] = NULL;
		mydat->windows[mydat->nsockfds] =
			createWindow(mydat->uiinf);
		mydat->windows[mydat->nsockfds]->userdata =
			(void*) &mydat->sockfds[mydat->nsockfds];
		mydat->windows[mydat->nsockfds]->event =
			clientEventCallback;
		mydat->styles[mydat->nsockfds] = malloc(
			sizeof(struct current_style));
		initStyle(mydat->styles[mydat->nsockfds]);
		mydat->nsockfds++;
	}
	else {
		char ch;
		if(read(mydat->sockfds[i], &ch, 1) <= 0){
			printf("Client %d disconnected.\n", i);
			mydat->sockfds[i] = -1;
			destroyWindow(mydat->uiinf, mydat->windows[i]);
			while(mydat->sockfds[mydat->nsockfds-1] == -1)
				mydat->nsockfds--;
			return;
		}
		if(mydat->isbinary[i]) saveByte(mydat, i, ch);
		else {
			if(mydat->bufindex[i] == MAX_COMMAND_LENGTH-1){
				ch = '\n';
			}
			mydat->buffers[i][mydat->bufindex[i]] =
				ch == '\n' ? 0 : ch;
			mydat->bufindex[i]++;
			if(ch == '\n' && !mydat->isbinary[i]){
				serverCommand(mydat, i, mydat->buffers[i]);
				mydat->bufindex[i] = 0;
			}
		}
	}
}
void cleanup_before_terminate(int signum)
{
	uicleanup(mydat.uiinf);
	exit(3);
}
int main(){
	struct sockaddr_un addr = {AF_UNIX, "/tmp/bananui.sock"};
	struct sigaction sigact;
	sigact.sa_flags = 0;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_handler = SIG_IGN;
	sigaction(SIGPIPE, &sigact, NULL);
	sigact.sa_handler = cleanup_before_terminate;
	sigaction(SIGINT, &sigact, NULL);
	sigaction(SIGTERM, &sigact, NULL);
	mydat.nsockfds = 1;
	mydat.sockfds[0] = socket(AF_UNIX, SOCK_STREAM, 0);
	if(mydat.sockfds[0] < 0){
		perror("socket");
		exit(5);
	}
	fcntl(mydat.sockfds[0], F_SETFD, FD_CLOEXEC);
	if(bind(mydat.sockfds[0], (struct sockaddr*) &addr, sizeof(addr)) <
		0)
	{
		perror("Failed to bind to /tmp/bananui.sock");
	}
	if(listen(mydat.sockfds[0], MAX_WAITING_CLIENTS) < 0){
		perror("listen");
		exit(5);
	}
	chmod("/tmp/bananui.sock", 0666);
	if(fork() == 0){
		execl("/usr/bin/bananui-login", "bananui-login", NULL);
		perror("/usr/bin/bananui-login");
		exit(2);
	}
	mydat.uiinf = getui();
	uiLoop(mydat.uiinf, mydat.sockfds, &mydat.nsockfds,
		fdCallback, &mydat);
	uicleanup(mydat.uiinf);
	return 0;
}
