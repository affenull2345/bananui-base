# Package bananui-base

This package is part of [bananian](https://gitlab.com/affenull2345/bananian).
To build it, please use the Makefile in that repository (see its README for
more information).

### Banana icon

The banana icon is in the public domain (license: [CC0](https://creativecommons.org/publicdomain/zero/1.0/)).
