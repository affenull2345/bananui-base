/* About screen for Bananui
 * Copyright: see string below */
#include <linux/input-event-codes.h>
#include <bananui/bananui.h>

static const char copyright[] =
"\n"
"Bananian v" VERSION "\n"
"\n"
"Bananui user interface\n"
"Copyright (C) 2020-2021\n"
"Affe Null <affenull2345@gmail.com>\n"
"\n"
"This program is free\n"
"software: you can\n"
"redistribute it \n"
"and/or modify it under\n"
"the terms of the GNU\n"
"General Public License\n"
"as published by the\n"
"Free Software\n"
"Foundation, either\n"
"version 3 of the\n"
"License, or any later\n"
"version.\n"
"\n"
"This program is\n"
"distributed in the\n"
"hope that it will\n"
"be useful, but\n"
"WITHOUT ANY WARRANTY;\n"
"without even the\n"
"implied warranty of\n"
"MERCHANTABILITY or\n"
"FITNESS FOR A\n"
"PARTICULAR PURPOSE.\n"
"See the GNU General\n"
"Public License for more\n"
"details.\n"
"You should have\n"
"received a copy of\n"
"the GNU General\n"
"Public License along\n"
"with this program.\n"
"If not, see\n"
"<https://www.gnu.org/licenses/>.\n"
"\n"
"On Debian systems, the\n"
"complete text of the\n"
"GNU General Public\n"
"License can be found\n"
"in \"/usr/share/common-licenses/GPL-3\".\n"
"\n"
"The following library\n"
"is part of the Bananui\n"
"user interface:\n"
" inih -- simple .INI\n"
"file parser\n"
"\n"
"Copyright (C)\n"
"2009-2020, Ben Hoyt\n"
"inih is released under\n"
"the New BSD license.\n"
"Go to the project home\n"
"page for more info:\n"
"https://github.com/benhoyt/inih\n";

int eventCallback(bEventType type, bEventData *evinfo)
{
	if(type == BEV_EXIT
		|| (type == BEV_KEYDOWN && evinfo->val == KEY_ENTER))
	{
		return 0;
	}
	return -1;
}

int main()
{
	bWindow *wnd;
	wnd = bCreateWnd();
	if(!wnd) return 1;
	bSetWindowTitle(wnd, "@help-about@About");
	bStartLine(wnd);
	bCreateLabel(wnd, "It's ", TA_LEFT, NO_WIDGET);
	bCreateIcon(wnd, "debian-logo", TA_LEFT, 48, 48, NO_WIDGET);
	bCreateLabel(wnd, " on ", TA_LEFT, NO_WIDGET);
	bCreateIcon(wnd, "banana", TA_LEFT, 48, 48, NO_WIDGET);
	bEndLine(wnd);
	bCreateLabel(wnd, copyright, TA_LEFT, NO_WIDGET);
	bSetSoftkeys(wnd, "", "CLOSE", "");
	bSetEventCallback(wnd, eventCallback);
	bRefreshWnd(wnd, 0);

	return bEventLoop(wnd);
}
