/*
	The BananUI user interface. For the socket and command implementation,
	see uiserv.c.
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <linux/fb.h>
#include <linux/input.h>
#include "gr/gr.h"
#include "gr/extra.h"
#include "ui.h"
#include "icon.h"
#include "keymap.h"
#include "font.h"

#define KEY_SLIDE 249

#define BG_R 0xA0
#define BG_G 0xA0
#define BG_B 0x80

#define LED_BRIGHTNESS_FILE "/sys/class/leds/button-backlight/brightness"

#define USB_POWER_STATE_FILE \
	"/sys/devices/soc.0/78d9000.usb/power/runtime_status"

/* ================================== TEXT ================================== */

static int getxoffset(enum ui_text_align ta, const char *text, int xres)
{
	int offset;
	switch(ta){
		case UI_TA_CENTER:
			offset = xres / 2 - font_get_str_width(text) / 2;
			if(offset < 0) return 0;
			return offset;
		case UI_TA_RIGHT:
			offset = xres - font_get_str_width(text) - 1;
			if(offset < 0) return 0;
			return offset;
		default:
			return 0;
	}
}

static int text(struct uiinfo *uiinf, enum ui_text_align ta, int initx, int y,
	int xend, const char *txt, int r, int g, int b, int a, int nowrap)
{
	char ch;
	int height = 0, lineheight, x;
	x = initx + getxoffset(ta, txt, xend - initx);
	lineheight = font_get_default_min_line_height();
	while((ch = *txt++)){
		int charheight, charwidth;
		charwidth = font_get_char_width(ch);
		if(x+charwidth >= xend){
			if(nowrap) return height + lineheight;
			x = initx + getxoffset(ta, txt-1, xend - initx);
			y += lineheight;
			height += lineheight;
			lineheight = font_get_default_min_line_height();
		}
		charheight = font_get_char_height(ch);
		if(charheight > lineheight) lineheight = charheight;
		font_draw_character(uiinf->fbinf, x, y, ch, r, g, b, a);
		x += charwidth;
	}
	return height + lineheight;
}

/* ================================= WIDGETS ================================ */

static void box(struct uiinfo *uiinf, int x, int y, int h, int w,
	int br, int bg, int bb, int bgr, int bgg, int bgb)
{
	drawRect(uiinf->fbinf, y, x, h, w, br, bg, bb, 255);
	drawRect(uiinf->fbinf, y+2, x+2, h-4, w-4, bgr, bgg, bgb,
		255);
}

static void displayLabel(struct uiinfo *uiinf, struct ui_widget *widget){
	char *txt;
	txt = (char*) widget->data;
	if(widget->type != UI_LABEL) return;
	widget->height = text(uiinf, widget->textalign, widget->x,
		widget->y, widget->rightend, txt,
		get_red(widget->style.fgcolor),
		get_green(widget->style.fgcolor),
		get_blue(widget->style.fgcolor),
		get_alpha(widget->style.fgcolor), 0);
}
static void displayIcon(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget)
{
	int offset;
	struct icon_widget_data *data;
	data = widget->data;
	if(widget->type != UI_ICON) return;
	switch(widget->textalign){
		case UI_TA_CENTER:
			offset = widget->rightend / 2 -
				data->width / 2;
			break;
		case UI_TA_RIGHT:
			offset = widget->rightend - data->width;
			break;
		default:
			offset = 0;
			break;
	}
	widget->height = data->height;
	showIcon(uiinf, widget->x+offset, widget->y, data->width, data->name,
		win->icontheme);
}
static void displayImage(struct uiinfo *uiinf, struct ui_widget *widget){
	int offset, width, height, i, j;
	unsigned int *data;
	unsigned char *chardata;
	chardata = widget->data;
	data = (unsigned int*) (chardata+2);
	width = chardata[0];
	height = chardata[1];
	if(widget->type != UI_IMAGE) return;
	switch(widget->textalign){
		case UI_TA_CENTER:
			offset = widget->rightend / 2 - width / 2;
			break;
		case UI_TA_RIGHT:
			offset = widget->rightend - width;
			break;
		default:
			offset = 0;
			break;
	}
	for(i = 0; i < height; i++){
		for(j = 0; j < width; j++){
			setPixel(uiinf->fbinf, widget->y+i,
				offset+widget->x+j, data[i*width+j] &0xff,
				(data[i*width+j] >> 8) & 0xff,
				(data[i*width+j] >> 16) & 0xff,
				(data[i*width+j] >> 24) & 0xff);
		}
	}
	widget->height = height;
}
static void displayButton(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget)
{
	char *txt;
	int hasicon = 0, textheight;
	int border_red, border_green, border_blue;
	if(widget->type != UI_BUTTON && widget->type != UI_FOBUTTON)
		return;
	if(widget->type == UI_FOBUTTON){
		border_red = get_red(widget->style.focolor);
		border_green = get_green(widget->style.focolor);
		border_blue = get_blue(widget->style.focolor);
	}
	else {
		border_red = get_red(widget->style.bocolor);
		border_green = get_green(widget->style.bocolor);
		border_blue = get_blue(widget->style.bocolor);
	}
	txt = (char*) widget->data;
	/* FIXME Text is drawn twice, once to get height and then on top of
	 * the box */
	if(txt[0] != '@'){
		textheight = text(uiinf, UI_TA_LEFT,
			widget->x + (hasicon ? 64 : 10),
			widget->y + 21, widget->rightend-9, txt,
			get_red(widget->style.fgcolor),
			get_green(widget->style.fgcolor),
			get_blue(widget->style.fgcolor),
			get_alpha(widget->style.fgcolor), 0);
		box(uiinf, widget->x, widget->y, 42+textheight,
			widget->rightend - widget->x,
			border_red, border_green, border_blue,
			get_red(widget->style.bgcolor),
			get_green(widget->style.bgcolor),
			get_blue(widget->style.bgcolor));
	}
	else {
		int i;
		char *iconname, *oldtxt = txt;
		/* Count first */
		for(i = 1; txt[i] && txt[i] != '@'; i++) {}
		iconname = malloc(i * sizeof(char));
		txt += i;
		if(txt[0] == '@') txt++;
		/* Then copy */
		for(i = 1; oldtxt[i] && oldtxt[i] != '@'; i++){
			iconname[i-1] = oldtxt[i];
		}
		iconname[i-1] = 0;
		textheight = text(uiinf, UI_TA_LEFT,
			widget->x + (hasicon ? 64 : 10),
			widget->y + 21, widget->rightend-9, txt,
			get_red(widget->style.fgcolor),
			get_green(widget->style.fgcolor),
			get_blue(widget->style.fgcolor),
			get_alpha(widget->style.fgcolor), 0);
		box(uiinf, widget->x, widget->y, 42+textheight,
			widget->rightend - widget->x,
			border_red, border_green, border_blue,
			get_red(widget->style.bgcolor),
			get_green(widget->style.bgcolor),
			get_blue(widget->style.bgcolor));
		showIcon(uiinf, widget->x+6, widget->y+6, 48, iconname,
			win->icontheme);
		free(iconname);
		hasicon = 1;
	}
	text(uiinf, UI_TA_LEFT, widget->x + (hasicon ? 64 : 10), widget->y + 21,
		widget->rightend-9, txt,
		get_red(widget->style.fgcolor),
		get_green(widget->style.fgcolor),
		get_blue(widget->style.fgcolor),
		get_alpha(widget->style.fgcolor), 0);
	widget->height = 42+textheight;
}

static void displayInput(struct uiinfo *uiinf, struct ui_widget *widget)
{
	char *txt, tmp;
	int border_red, border_green, border_blue, scrolloffset, cursorpos;
	cursorpos = ((unsigned char *)widget->data)[1];
	scrolloffset = ((unsigned char *)widget->data)[0];
	if(widget->type != UI_INPUT && widget->type != UI_FOINPUT)
		return;
	if(widget->type == UI_FOINPUT){
		border_red = get_red(widget->style.focolor);
		border_green = get_green(widget->style.focolor);
		border_blue = get_blue(widget->style.focolor);
	}
	else {
		border_red = get_red(widget->style.bocolor);
		border_green = get_green(widget->style.bocolor);
		border_blue = get_blue(widget->style.bocolor);
	}
	txt = ((char*)widget->data)+2;
	box(uiinf, widget->x, widget->y, 60, widget->rightend - widget->x,
		border_red, border_green, border_blue, 255, 255, 255);
	if(widget->inputtype != UI_INP_PASSWORD){
		text(uiinf, UI_TA_LEFT, widget->x + 10, widget->y + 20,
			widget->rightend-9, txt+scrolloffset, 0, 0, 0, 255, 1);
	}
	else {
		char *fakestring;
		int i;
		fakestring = malloc((strlen(txt+scrolloffset)+1) *
			sizeof(char));
		for(i = 0; i < strlen(txt+scrolloffset); i++){
			if(i == cursorpos - scrolloffset - 1 &&
				uiinf->curkey >= 0)
			{
				fakestring[i] = txt[scrolloffset+i];
			}
			else {
				fakestring[i] = '*';
			}
		}
		fakestring[i] = 0;
		text(uiinf, UI_TA_LEFT, widget->x + 10, widget->y + 20,
			widget->rightend-9, fakestring, 0, 0, 0, 255, 1);
		free(fakestring);
	}
	if(widget->type == UI_FOINPUT){
		tmp = txt[cursorpos];
		txt[cursorpos] = 0;
		drawRect(uiinf->fbinf, widget->y+17, widget->x + 10 +
			font_get_str_width(txt+scrolloffset),
			20, 1, 0, 0, 0, 255);
		txt[cursorpos] = tmp;
	}
	widget->height = 60;
}
int getApproximateWidth(struct uiinfo *uiinf, struct ui_widget *widget,
	int n)
{
	switch(widget->type){
		case UI_BUTTON:
		case UI_FOBUTTON:
		case UI_INPUT:
		case UI_FOINPUT:
			return getxres(uiinf->fbinf) / n;
		case UI_LABEL:
			return font_get_str_width((char*)widget->data);
		case UI_IMAGE:
			return ((unsigned char*)widget->data)[0];
		case UI_ICON:
			return ((struct icon_widget_data*)widget->data)->
				width;
		case UI_CBX:
			fprintf(stderr, "Nested CBX is not possible!\n");
	}
	return 0;
}
void addWidgetToCbx(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *cbx, struct ui_widget *widget, int focusable)
{
	struct ui_cbx_info *cbxi = cbx->data;
	widget->cbx = cbx;
	if(cbxi->last){
		cbxi->last->right = widget;
	}
	else {
		cbxi->first = widget;
	}
	widget->right = NULL;
	widget->left = cbxi->last;
	if(focusable){
		widget->focusable = 1;
		if(cbxi->lastfoc){
			cbxi->lastfoc->rightfoc = widget;
		}
		else {
			cbxi->firstfoc = widget;
		}
		widget->leftfoc = cbxi->lastfoc;
		widget->rightfoc = NULL;
		cbxi->lastfoc = widget;
	}
	win->widgets[win->nextwidgetid] = widget;
	widget->id = win->nextwidgetid;
	win->nextwidgetid++;
	cbxi->last = widget;
	cbxi->numwidgets++;
}
static void displayCbx(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *cbx)
{
	struct ui_widget *widget;
	struct ui_cbx_info *cbxi = cbx->data;
	int x = 0, height = 0;
	for(widget = cbxi->first; widget;
			widget = widget->right)
	{
		widget->y = cbx->y;
		widget->x = x;
		widget->rightend = x += getApproximateWidth(uiinf, widget,
			cbxi->numwidgets);
		redrawWidget(uiinf, win, widget);
		if(widget->height > height) height = widget->height;
		widget->nextfoc = cbx->nextfoc;
		widget->prevfoc = cbx->prevfoc;
	}
	cbx->height = height;
}

static void displayWidget(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget)
{
	switch(widget->type){
		case UI_BUTTON:
		case UI_FOBUTTON:
			displayButton(uiinf, win, widget);
			break;
		case UI_IMAGE:
			displayImage(uiinf, widget);
			break;
		case UI_ICON:
			displayIcon(uiinf, win, widget);
			break;
		case UI_LABEL:
			displayLabel(uiinf, widget);
			break;
		case UI_INPUT:
		case UI_FOINPUT:
			displayInput(uiinf, widget);
			break;
		case UI_CBX:
			displayCbx(uiinf, win, widget);
			break;
		default:
			fprintf(stderr, "Unknown widget type %d\n",
				widget->type);
	}
}

void redrawWidget(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget)
{
	widget->y += win->actual_yoffset;
	displayWidget(uiinf, win, widget);
	drawRect(uiinf->fbinf, widget->y,
		widget->x, widget->height, widget->rightend - widget->x,
		get_red(widget->style.bgcolor),
		get_green(widget->style.bgcolor),
		get_blue(widget->style.bgcolor), 255);
	displayWidget(uiinf, win, widget);
	widget->y -= win->actual_yoffset;
}

void destroyWidget(struct uiinfo *uiinf, struct ui_widget *widget)
{
	if(widget->freedata) free(widget->data);
	free(widget);
}


/* ================================= FOCUS ================================== */

static void redrawWindowBuffer(struct uiinfo *uiinf, struct ui_window *win);

void focus(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget)
{
	if(win->focused){
		switch(win->focused->type){
			case UI_FOBUTTON:
				win->focused->type = UI_BUTTON;
				break;
			case UI_FOINPUT:
				win->focused->type = UI_INPUT;
				break;
			case UI_CBX:
				break;
			default:
				fprintf(stderr,
					"win->focused is not focused\n");
		}
	}
	if(!widget){
		win->focused = NULL;
		return;
	}
	if(widget->y < 0){
		win->scrolloffset += widget->y;
	}
	else if(widget->y + widget->height >
		win->height)
	{
		win->scrolloffset += (widget->y + widget->height) -
			win->height;
	}
	switch(widget->type){
		case UI_BUTTON:
			widget->type = UI_FOBUTTON;
			break;
		case UI_INPUT:
			widget->type = UI_FOINPUT;
			break;
		case UI_CBX: {
			struct ui_widget *subwidget;
			int i;
			subwidget = ((struct ui_cbx_info*)widget->data)->
				firstfoc;
			if(!subwidget){
				focus(uiinf, win, widget->nextfoc);
				return;
			}
			for(i = 0; subwidget->right &&
				i < win->cbx_focus_index; i++)
			{
				subwidget = subwidget->right;
			}
			focus(uiinf, win, subwidget);
			return;
		}
		default:
			fprintf(stderr,
				"Widget already focused or not focusable\n");
	}
	win->focused = widget;
	win->event(win->userdata, widget->id, UI_EVENT_FOCUS);
}

/*================================= TIMEOUTS =================================*\
|*Timeouts are currently not used, because transitions no longer rely on them.*|
\*============================================================================*/

static time_t timeout_getSeconds(struct timeout *to)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	/* One second for the clock if there are no timeouts */
	if(!to) return 1;
	if(to->end_time.tv_sec < tv.tv_sec || (to->end_time.tv_sec ==
		tv.tv_sec && to->end_time.tv_usec < tv.tv_usec))
	{
		return 0;
	}
	return to->end_time.tv_sec - tv.tv_sec -
		(to->end_time.tv_usec < tv.tv_usec ? 1 : 0);
}

static suseconds_t timeout_getUsecs(struct timeout *to)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	if(!to) return 0;
	if(to->end_time.tv_sec < tv.tv_sec || (to->end_time.tv_sec ==
		tv.tv_sec && to->end_time.tv_usec < tv.tv_usec))
	{
		return 0;
	}
	if(to->end_time.tv_usec < tv.tv_usec)
	{
		return (1000000 - tv.tv_usec) + to->end_time.tv_usec;
	}
	return to->end_time.tv_usec - tv.tv_usec;
}

static void handleTimeouts(struct timeout **to_dest)
{
	struct timeout *to;
	struct timeval tv;
	gettimeofday(&tv, NULL);
	to = *to_dest;
	while(to && to->end_time.tv_usec <= tv.tv_usec && to->end_time.tv_sec <=
		tv.tv_sec)
	{
		to->cb(to->userdata);
		*to_dest = to->next;
		free(to);
		to = *to_dest;
	}
}

void scheduleTimeout(struct uiinfo *uiinf, struct timeval length,
	timeout_callback cb, void *userdata)
{
	struct timeout **to_dest;
	struct timeout *to;
	struct timeval end_time;
	gettimeofday(&end_time, NULL);
	end_time.tv_sec += length.tv_sec;
	if((1000000 - end_time.tv_usec - length.tv_usec) < 0){
		/* tv_usec should not be greater than 1000000 */
		end_time.tv_sec++;
		end_time.tv_usec -= (1000000 - length.tv_usec);
	}
	for(to_dest = &(uiinf->timeouts); *to_dest &&
		((*to_dest)->end_time.tv_sec < end_time.tv_sec ||
			((*to_dest)->end_time.tv_sec == end_time.tv_sec &&
			 	(*to_dest)->end_time.tv_usec < end_time.tv_usec)
		); to_dest = &((*to_dest)->next));

	to = malloc(sizeof(struct timeout));
	to->end_time = end_time;
	to->cb = cb;
	to->userdata = userdata;
	to->next = *to_dest;
	*to_dest = to;
}

/* =============================== TRANSITIONS ============================== */

typedef void (*transition_callback)(void*);

struct transition {
	suseconds_t speed;
	int percentage, buf1height, buf2height, yoffset;
	uint32_t *buffer1, *buffer2, *outbuffer;
	enum transition_style style;
	transition_callback completion_callback;
	void *userdata;
	struct timeval oldtime;
	struct transition *next, *prev;
};

static void transitionRedrawWindowCallback(void *userdata)
{
	struct uiinfo *uiinf = userdata;
	redrawWindow(uiinf, uiinf->curwindow);
}

static void stepTransition(struct uiinfo *uiinf, struct transition *trans)
{
	struct timeval tv, diff;
	suseconds_t timediff;
	int percentage_diff;
	gettimeofday(&tv, NULL);
	timersub(&tv, &(trans->oldtime), &diff);
	timediff = diff.tv_sec ? 1000000 : diff.tv_usec;
	percentage_diff = timediff * 100 / trans->speed;
	if(percentage_diff <= 0) return; /* Too little time spent */
	trans->oldtime = tv;
	trans->percentage += percentage_diff;
	if(trans->percentage >= 100){
		if(trans == uiinf->transitions) uiinf->transitions = NULL;
		trans->completion_callback(trans->userdata);
		if(trans->style == TRANS_CROSS)
			free(trans->outbuffer);
		if(trans->prev) trans->prev->next = trans->next;
		if(trans->next) trans->next->prev = trans->prev;
		free(trans);
		return;
	}
	switch(trans->style){
		case TRANS_POPUP:
			fbcpy(uiinf->fbinf, trans->buffer1,
				trans->yoffset, 0,
				trans->buf1height,
				getxres(uiinf->fbinf));
			fbcpy(uiinf->fbinf, trans->buffer2,
				trans->buf2height -
				(trans->percentage *
				trans->buf2height / 100) +
				trans->yoffset, 0,
				trans->buf2height,
				getxres(uiinf->fbinf));
			break;
		case TRANS_POPDOWN:
			fbcpy(uiinf->fbinf, trans->buffer1,
				trans->yoffset, 0,
				trans->buf1height,
				getxres(uiinf->fbinf));
			fbcpy(uiinf->fbinf, trans->buffer2,
				(trans->percentage *
				trans->buf2height / 100) +
				trans->yoffset, 0,
				trans->buf2height,
				getxres(uiinf->fbinf));
			break;
		case TRANS_CROSS:
			blend_buffers(trans->buffer1,
				trans->buffer2, BLEND_STYLE_CROSSFADE,
				trans->buf1height,
				getxres(uiinf->fbinf),
				trans->percentage,
				trans->outbuffer);
			fbcpy(uiinf->fbinf, trans->outbuffer,
				trans->yoffset,
				0, trans->buf1height,
				getxres(uiinf->fbinf));
			break;
		default:
			fprintf(stderr, "Transition not implemented\n");
	}
	refreshScreen(uiinf->fbinf);
}

static void scheduleTransition(struct uiinfo *uiinf,
	enum transition_style style, suseconds_t speed, uint32_t *buffer1,
	uint32_t *buffer2, int buf1height, int buf2height, int yoffset,
	transition_callback completion_callback, void *userdata)
{
	struct transition *trans;
	/* We will not draw anything if there already is a transition */
	if(uiinf->transitions || style == TRANS_NONE){
		completion_callback(userdata);
		return;
	}
	trans = malloc(sizeof(struct transition));
	trans->speed = speed;
	trans->style = style;
	trans->percentage = 0;
	if(style == TRANS_CROSS) trans->outbuffer =
		malloc(4 * getyres(uiinf->fbinf) * getxres(uiinf->fbinf));
	trans->buffer1 = buffer1;
	trans->buffer2 = buffer2;
	trans->buf1height = buf1height;
	trans->buf2height = buf2height;
	trans->yoffset = yoffset;
	trans->completion_callback = completion_callback;
	trans->userdata = userdata;
	trans->next = uiinf->transitions;
	if(trans->next) trans->next->prev = trans;
	trans->prev = NULL;
	gettimeofday(&(trans->oldtime), NULL);
	uiinf->transitions = trans;
	stepTransition(uiinf, trans);
}

/* ================================= WINDOWS ================================ */

static void showTopPanel(struct uiinfo *uiinf)
{
	struct tm *curtime;
	char timestr[6];
	time_t t;
	int bgr = 0, bgg = 0, bgb = 0, fgr = 255, fgg = 255, fgb = 255;
	if(uiinf->curwindow){
		bgr = uiinf->curwindow->tpbgr;
		bgg = uiinf->curwindow->tpbgg;
		bgb = uiinf->curwindow->tpbgb;
		fgr = uiinf->curwindow->tpfgr;
		fgg = uiinf->curwindow->tpfgg;
		fgb = uiinf->curwindow->tpfgb;
	}
	t = time(NULL);
	curtime = localtime(&t);
	if(!curtime){
		perror("localtime");
	}
	drawRect(uiinf->fbinf, 0, 0, 25, getxres(uiinf->fbinf),
		bgr, bgg, bgb, 255);
	if(strftime(timestr, 6, "%R", curtime)){
		text(uiinf, UI_TA_LEFT, 3, 3, getxres(uiinf->fbinf),
			timestr, fgr, fgg, fgb, 255, 1);
	}
}
static void showSoftkeys(struct uiinfo *uiinf, struct ui_window *win)
{
	int text_y, rightend;
	drawRect(uiinf->fbinf, getyres(uiinf->fbinf) - 25, 0, 25,
		getxres(uiinf->fbinf), win->skbgr, win->skbgg,
		win->skbgb, win->skbga);
	text_y = getyres(uiinf->fbinf) - 20;
	rightend = getxres(uiinf->fbinf) - 5;
	text(uiinf, UI_TA_LEFT, 5, text_y, rightend, win->current_skl,
		win->skfgr, win->skfgg, win->skfgb, 255, 1);
	text(uiinf, UI_TA_CENTER, 5, text_y, rightend, win->current_skc,
		win->skfgr, win->skfgg, win->skfgb, 255, 1);
	text(uiinf, UI_TA_RIGHT, 5, text_y, rightend, win->current_skr,
		win->skfgr, win->skfgg, win->skfgb, 255, 1);
}

struct destroy_window_data {
	struct uiinfo *uiinf;
	struct ui_window *win, *nextcurwindow;
	uint32_t *tmpbuf;
};

static void transitionDestroyWindowCallback(void *userdata)
{
	int i;
	struct destroy_window_data *data = userdata;
	if(!data->nextcurwindow) free(data->tmpbuf);
	if(data->win == data->uiinf->curwindow){
		data->uiinf->curwindow = data->win->parent;
		if(!data->uiinf->curwindow){
			int i;
			for(i = 0; i < MAX_WINDOWS; i++){
				if(i == data->win->id) continue;
				if(data->uiinf->windows[i]){
					data->uiinf->curwindow =
						data->uiinf->windows[i];
					break;
				}
			}
		}
	}
	for(i = 0; i < MAX_WIDGETS; i++){
		if(data->win->widgets[i])
			destroyWidget(data->uiinf, data->win->widgets[i]);
	}
	data->uiinf->windows[data->win->id] = NULL;
	while(data->uiinf->windows[data->uiinf->nextwindowid-1] == NULL)
		data->uiinf->nextwindowid--;
	redrawWindow(data->uiinf, data->uiinf->curwindow);
	free(data->win);
	free(data);
}

void destroyWindow(struct uiinfo *uiinf, struct ui_window *win)
{
	uint32_t *tmpbuf;
	struct destroy_window_data *data;
	data = malloc(sizeof(struct destroy_window_data));
	data->nextcurwindow = uiinf->curwindow;
	if(win == data->nextcurwindow){
		data->nextcurwindow = win->parent;
		if(!data->nextcurwindow){
			int i;
			for(i = 0; i < MAX_WINDOWS; i++){
				if(i == win->id) continue;
				if(uiinf->windows[i]){
					data->nextcurwindow = uiinf->windows[i];
					break;
				}
			}
		}
	}
	if(data->nextcurwindow){
		redrawWindowBuffer(uiinf, data->nextcurwindow);
		tmpbuf = data->nextcurwindow->buffer;
	}
	else {
		size_t bufsize = 4 * getyres(uiinf->fbinf) *
			getxres(uiinf->fbinf);
		int i;
		tmpbuf = malloc(bufsize);
		for(i = 0; i < (bufsize/4); i++){
			tmpbuf[i] = 0x000000ff;
		}
	}
	redrawWindowBuffer(uiinf, win);
	/*for(i = 0; i <= 7; i++){
		fbcpy(uiinf->fbinf, tmpbuf, 0, 0, uiinf->curwindow ?
			uiinf->curwindow->height : getyres(uiinf->fbinf),
			getxres(uiinf->fbinf));
		fbcpy(uiinf->fbinf, win->buffer,
			win->actual_yoffset + (i * 40), 0, win->height,
			getxres(uiinf->fbinf));
		refreshScreen(uiinf->fbinf);
	}*/
	data->tmpbuf = tmpbuf;
	data->uiinf = uiinf;
	data->win = win;
	if(!win->hidetoppanel){
		showTopPanel(uiinf);
	}
	scheduleTransition(uiinf, TRANS_POPDOWN, 500000, tmpbuf, win->buffer,
		data->nextcurwindow ? data->nextcurwindow->height :
		getyres(uiinf->fbinf), win->height, win->hidetoppanel ? 0 : 25,
		transitionDestroyWindowCallback, data);
}

void clearWindow(struct uiinfo *uiinf, struct ui_window *win)
{
	int i;
	win->focused = NULL;
	for(i = 0; i < MAX_WIDGETS; i++){
		if(win->widgets[i]) destroyWidget(uiinf, win->widgets[i]);
		win->widgets[i] = NULL;
	}
	win->firstfocusable = NULL;
	win->lastfocusable = NULL;
	win->firstwidget = NULL;
	win->lastwidget = NULL;
	win->nextwidgetid = 0;
	win->scrolloffset = 0;
}

static void initWindowStyle(struct ui_window *win){
	win->skbgr = win->skbgg = win->skbgb = 0x80;
	win->skbga = 255;
	win->skfgr = win->skfgg = win->skfgb = 0;
	win->bgr = win->bgg = win->bgb = 0xA0;
	win->tpbgr = win->tpbgg = win->tpbgb = 0;
	win->tpfgr = win->tpfgg = win->tpfgb = 255;
}

struct ui_window *createWindow(struct uiinfo *uiinf)
{
	struct ui_window *win;
	int i;
	uint32_t *tmpbuf;
	if(uiinf->nextwindowid == MAX_WINDOWS){
		fprintf(stderr, "Too many windows!\n");
		uicleanup(uiinf);
		exit(4);
	}
	win = malloc(sizeof(struct ui_window));
	initWindowStyle(win);
	uiinf->windows[uiinf->nextwindowid] = win;
	win->id = uiinf->nextwindowid;
	uiinf->nextwindowid++;
	win->cbx_focus_index = 0;
	for(i = 0; i < MAX_WIDGETS; i++){
		win->widgets[i] = NULL;
	}
	win->firstwidget = NULL;
	win->lastwidget = NULL;
	win->firstfocusable = NULL;
	win->lastfocusable = NULL;
	win->focused = NULL;
	win->parent = NULL;
	win->scrolloffset = 0;
	win->maxscroll = -1; /* tell redrawWindow to compute it */
	win->nextwidgetid = 0;
	win->hidetoppanel = 0;
	win->hideskpanel = 0;
	win->current_skl[0] = win->current_skc[0] = win->current_skr[0] = 0;
	win->buffer = malloc(4 * getyres(uiinf->fbinf) * getxres(uiinf->fbinf));
	win->oldbuffer = NULL;
	win->title[0] = '\0';
	strcpy(win->icontheme, "default");
	redrawWindowBuffer(uiinf, win);
	if(uiinf->curwindow){
		redrawWindowBuffer(uiinf, uiinf->curwindow);
		tmpbuf = uiinf->curwindow->buffer;
	}
	else {
		size_t bufsize = 4 * getyres(uiinf->fbinf) *
			getxres(uiinf->fbinf);
		int i;
		tmpbuf = malloc(bufsize);
		for(i = 0; i < (bufsize/4); i++){
			tmpbuf[i] = 0x000000ff;
		}
	}
	uiinf->curwindow = win;
	/*for(i = 0; i <= 7; i++){
		fbcpy(uiinf->fbinf, win->buffer,
			getyres(uiinf->fbinf) - (i * 40), 0, win->height,
			getxres(uiinf->fbinf));
		refreshScreen(uiinf->fbinf);
	}*/
	if(!win->hidetoppanel){
		showTopPanel(uiinf);
	}
	scheduleTransition(uiinf, TRANS_POPUP, 500000, tmpbuf, win->buffer,
		win->height, win->height, win->hidetoppanel ? 0 : 25,
		transitionRedrawWindowCallback, uiinf);
	return win;
}
static void redrawWindowBuffer(struct uiinfo *uiinf, struct ui_window *win)
{
	struct ui_widget *tmp;
	if(!win) return;
	win->actual_yoffset = 0;
	emulateFramebuffer(uiinf->fbinf, &(win->buffer), win->hidetoppanel ?
		getyres(uiinf->fbinf) : getyres(uiinf->fbinf) - 25,
		getxres(uiinf->fbinf));
	win->height = getyres(uiinf->fbinf);
	drawRect(uiinf->fbinf, 0, 0, win->height,
		getxres(uiinf->fbinf), win->bgr, win->bgg, win->bgb,
		255);
	if(win->firstwidget){
		win->firstwidget->y = -(win->scrolloffset);
	}
	for(tmp = win->firstwidget; tmp; tmp = tmp->next){
		redrawWidget(uiinf, win, tmp);
		if(tmp->next){
			tmp->next->y = tmp->y + tmp->height;
		}
		win->maxscroll = tmp->y + tmp->height +
			win->scrolloffset - win->height;
	}
	restoreFramebuffer(uiinf->fbinf, &(win->buffer));
}
struct window_with_uiinfo {
	struct ui_window *win;
	struct uiinfo *uiinf;
};
static void transitionRedrawWindowCallbackFree(void *userdata)
{
	struct window_with_uiinfo *data = userdata;
	free(data->win->oldbuffer);
	redrawWindow(data->uiinf, data->win);
	free(data);
}
void redrawWindowTransition(struct uiinfo *uiinf, struct ui_window *win,
	enum transition_style trans, suseconds_t speed)
{
	struct window_with_uiinfo *data;
	if(win != uiinf->curwindow) return;

	/* We don't want to free the window's buffer during another
	 * transition because that other transition may be using it */
	if(uiinf->transitions) return;


	win->oldbuffer = win->buffer;
	win->buffer = malloc(4 * getyres(uiinf->fbinf) * getxres(uiinf->fbinf));
	data = malloc(sizeof(struct window_with_uiinfo));
	data->win = win;
	data->uiinf = uiinf;
	redrawWindowBuffer(uiinf, win);
	scheduleTransition(uiinf, trans, speed, win->oldbuffer, win->buffer,
		win->height, win->height, win->hidetoppanel ? 0 : 25,
		transitionRedrawWindowCallbackFree, data);
}
void redrawWindow(struct uiinfo *uiinf, struct ui_window *win)
{
	if(!win && !uiinf->transitions){
		drawRect(uiinf->fbinf, 0, 0, getyres(uiinf->fbinf),
			getxres(uiinf->fbinf), 0, 0, 0, 255);
	}
	else if(win){
		redrawWindowBuffer(uiinf, win);
		if(win == uiinf->curwindow){
			if(!uiinf->transitions){
				fbcpy(uiinf->fbinf, win->buffer,
					win->hidetoppanel ? 0 : 25, 0,
					win->height, getxres(uiinf->fbinf));
			}
			if(!win->hideskpanel){
				win->height -= 25;
				win->maxscroll += 25;
				if(!uiinf->transitions)
					showSoftkeys(uiinf, win);
			}
			if(!win->hidetoppanel){
				win->actual_yoffset += 25;
				if(!uiinf->transitions)
					showTopPanel(uiinf);
			}
		}
	}
	refreshScreen(uiinf->fbinf);
}

void addWidget(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget, int focusable)
{
	if(win->nextwidgetid == MAX_WIDGETS){
		fprintf(stderr, "Too many widgets\n");
		return;
	}
	win->widgets[win->nextwidgetid] = widget;
	widget->id = win->nextwidgetid;
	win->nextwidgetid++;
	widget->focusable = 0;
	widget->x = 0;
	widget->next = NULL;
	widget->nextfoc = NULL;
	widget->right = NULL;
	widget->rightfoc = NULL;
	widget->leftfoc = NULL;
	widget->cbx = NULL;
	widget->prev = win->lastwidget;
	if(!win->firstwidget){
		win->firstwidget = widget;
	}
	else {
		win->lastwidget->next = widget;
	}
	win->lastwidget = widget;
	if(focusable){
		widget->focusable = 1;
		if(!win->firstfocusable){
			win->firstfocusable = widget;
		}
		else {
			win->lastfocusable->nextfoc = widget;
		}
		widget->prevfoc = win->lastfocusable;
		win->lastfocusable = widget;
		if(!win->focused){
			redrawWindowBuffer(uiinf, win);
			focus(uiinf, win, widget);
		}
	}
}

/* ================================= INPUT ================================== */

static int handleInputLeft(struct uiinfo *uiinf, struct ui_widget *inp)
{
	unsigned char *cursorpos, *scrolloffset;
	uiinf->curkey = -1;
	cursorpos = ((unsigned char*)inp->data)+1;
	scrolloffset = ((unsigned char*)inp->data);
	if(*cursorpos == 0) return 0;
	if(*scrolloffset == *cursorpos){
		(*scrolloffset)--;
	}
	(*cursorpos)--;
	redrawWidget(uiinf, uiinf->curwindow, inp);
	refreshScreen(uiinf->fbinf);
	return 1;
}
static int handleInputRight(struct uiinfo *uiinf, struct ui_widget *inp)
{
	unsigned char *cursorpos, *scrolloffset;
	char tmp, *text;
	uiinf->curkey = -1;
	scrolloffset = (unsigned char*)inp->data;
	cursorpos = ((unsigned char*)inp->data)+1;
	text = ((char*)inp->data)+2;
	if(*cursorpos == strlen(text)) return 0;
	(*cursorpos)++;
	tmp = text[*cursorpos];
	text[*cursorpos] = '\0';
	if(font_get_str_width(text + (*scrolloffset)) >
		(inp->rightend - inp->x - 20 /* Padding */))
	{
		(*scrolloffset)++;
	}
	text[*cursorpos] = tmp;
	redrawWidget(uiinf, uiinf->curwindow, inp);
	refreshScreen(uiinf->fbinf);
	return 1;
}

enum key_status_code { KEY_STAT_INSERT, KEY_STAT_REPLACE, KEY_STAT_NOP };

static char handleNumeric(struct uiinfo *uiinf, int n,
	enum key_status_code *stat)
{
	*stat = KEY_STAT_INSERT;
	switch(uiinf->inptype){
		case INPTYPE_NUMERIC:
			if(n < 10) return n + '0';
			if(n == 11){ /* # */
				uiinf->inptype = INPTYPE_ALPHA_LOWER;
				*stat = KEY_STAT_NOP;
				uiinf->curkey = -1;
				return 0;
			}
		case INPTYPE_ALPHA_UPPER:
		case INPTYPE_ALPHA_LOWER:
			if(n == 11){ /* # */
				uiinf->inptype = (
					uiinf->inptype == INPTYPE_ALPHA_LOWER ?
					INPTYPE_ALPHA_UPPER :
					INPTYPE_NUMERIC
				);
				*stat = KEY_STAT_NOP;
				uiinf->curkey = -1;
				return 0;
			}
			if(n != uiinf->curkey){
				uiinf->curkey = n;
				uiinf->keyindex = 0;
			}
			else {
				uiinf->keyindex++;
				if(!keymap[uiinf->curkey][uiinf->keyindex]){
					uiinf->keyindex = 0;
				}
				*stat = KEY_STAT_REPLACE;
			}
			{
				char ch;
				ch = keymap[uiinf->curkey][uiinf->keyindex];
				if(ch >= 'A' && ch <= 'Z' &&
					uiinf->inptype == INPTYPE_ALPHA_LOWER)
				{
					ch += 0x20;
				}
				return ch;
			}
	}
	return 0;
}

static int keyIn(struct uiinfo *uiinf, struct ui_window *win, int n)
{
	char tmp, ch, *text;
	int i;
	unsigned char *cursorpos, *scrolloffset;
	enum key_status_code stat;
	if(!win->focused || win->focused->type != UI_FOINPUT) return 0;
	ch = handleNumeric(uiinf, n, &stat);
	if(stat == KEY_STAT_NOP) return 0;
	scrolloffset = ((unsigned char*)win->focused->data);
	cursorpos = ((unsigned char*)win->focused->data)+1;
	text = ((char*)win->focused->data)+2;
	if(stat != KEY_STAT_REPLACE){
		for(i = 254; i >= *cursorpos; i--){
			text[i+1] = text[i];
		}
		text[255] = 0;
	}
	else {
		(*cursorpos)--;
	}
	text[*cursorpos] = ch;
	(*cursorpos)++;
	tmp = text[*cursorpos];
	text[*cursorpos] = '\0';
	if(font_get_str_width(text + (*scrolloffset)) >
		(win->focused->rightend - win->focused->x - 20 /* Padding */))
	{
		(*scrolloffset)++;
	}
	text[*cursorpos] = tmp;
	redrawWidget(uiinf, win, win->focused);
	refreshScreen(uiinf->fbinf);
	return 1;
}

static void letterInput(struct uiinfo *uiinf, struct ui_window *win,
	char key)
{
	char tmp, *text;
	int i;
	unsigned char *cursorpos, *scrolloffset;
	if(!win->focused || win->focused->type != UI_FOINPUT) return;
	scrolloffset = ((unsigned char*)win->focused->data);
	cursorpos = ((unsigned char*)win->focused->data)+1;
	text = ((char*)win->focused->data)+2;
	for(i = 254; i >= *cursorpos; i--){
		text[i+1] = text[i];
	}
	text[255] = 0;
	text[*cursorpos] = uiinf->shift ? key : (key - 'A' + 'a');
	(*cursorpos)++;
	tmp = text[*cursorpos];
	text[*cursorpos] = '\0';
	if(font_get_str_width(text + (*scrolloffset)) >
		(win->focused->rightend - win->focused->x - 20 /* Padding */))
	{
		(*scrolloffset)++;
	}
	text[*cursorpos] = tmp;
	redrawWidget(uiinf, win, win->focused);
	refreshScreen(uiinf->fbinf);
}

void handlePowerOff(struct uiinfo *uiinf)
{
#ifndef DESKTOP
	char usbstate[128];
	int ledfd, powerfd, usbfd, readresult;
	uiinf->on = 0;
	ledfd = open(LED_BRIGHTNESS_FILE, O_WRONLY);
	if(ledfd < 0){
		perror(LED_BRIGHTNESS_FILE);
		return;
	}
	write(ledfd, "0\n", 2);
	close(ledfd);
	blankScreen(uiinf->fbinf, 1);

	/* Don't suspend if USB is active - it will refuse to do that,
	 * resulting in an endless suspend loop */
	usbfd = open(USB_POWER_STATE_FILE, O_RDONLY);
	if(usbfd < 0){
		perror(USB_POWER_STATE_FILE);
		return;
	}
	readresult = read(usbfd, usbstate, sizeof(usbstate)-1);
	usbstate[readresult] = '\0';
	if(0 == strncmp(usbstate, "active", 6)) return;

	powerfd = open("/sys/power/state", O_WRONLY);
	if(powerfd < 0){
		perror("/sys/power/state");
		return;
	}
	write(powerfd, "mem\n", 4);
	close(powerfd);
#endif
}
void handlePowerOn(struct uiinfo *uiinf)
{
#ifndef DESKTOP
	int ledfd;
	uiinf->on = 1;
	ledfd = open(LED_BRIGHTNESS_FILE, O_WRONLY);
	if(ledfd < 0){
		perror(LED_BRIGHTNESS_FILE);
		return;
	}
	write(ledfd, "40\n", 4);
	close(ledfd);
	blankScreen(uiinf->fbinf, 0);
#endif
}
static void handleSlideFullopen(struct uiinfo *uiinf)
{
}
struct window_list_data {
	struct ui_window *win;
	struct uiinfo *uiinf;
	int windowids[MAX_WINDOWS], prevcur;
};
static void closeWindowList(struct window_list_data *listdata,
	struct ui_window *open)
{
	if(open == NULL) open = listdata->uiinf->windows[listdata->prevcur];
	listdata->win->parent = open;
	destroyWindow(listdata->uiinf, listdata->win);
	listdata->uiinf->windowlist = NULL;
	listdata->win->userdata = NULL;
	redrawWindow(listdata->uiinf, open);
	free(listdata);
}
static void windowListEventCallback(void *data, int id, enum ui_event evt)
{
	struct window_list_data *listdata = data;
	if(!listdata) return;
	if(evt == UI_EVENT_CLICK){
		closeWindowList(listdata, listdata->uiinf->windows[
			listdata->windowids[id]]);
	}
	else if(evt == UI_EVENT_EXIT){
		closeWindowList(listdata, listdata->uiinf->windows[
			listdata->prevcur]);
	}
}
static void showWindowList(struct uiinfo *uiinf)
{
	struct ui_widget *btn;
	struct window_list_data *listdata;
	int i;
	if(uiinf->windowlist){
		uiinf->curwindow = uiinf->windowlist;
		redrawWindow(uiinf, uiinf->windowlist);
		return;
	}
	listdata = malloc(sizeof(struct window_list_data));
	listdata->prevcur = uiinf->curwindow->id;
	listdata->win = createWindow(uiinf);
	uiinf->windowlist = listdata->win;
	listdata->uiinf = uiinf;
	strcpy(listdata->win->title, "Window list");
	listdata->win->bgr = 0xE0;
	listdata->win->bgg = 0xE0;
	listdata->win->bgb = 0xE0;
	listdata->win->skbgr = 0xC0;
	listdata->win->skbgg = 0xC0;
	listdata->win->skbgb = 0xC0;
	listdata->win->event = windowListEventCallback;
	listdata->win->userdata = listdata;
	for(i = 0; i < uiinf->nextwindowid; i++){
		if(uiinf->windows[i] && uiinf->windows[i] != listdata->win){
			btn = malloc(sizeof(struct ui_widget));
			btn->type = UI_BUTTON;
			btn->rightend = getxres(uiinf->fbinf);
			btn->data = uiinf->windows[i]->title;
			btn->freedata = 0;
			btn->style.fgcolor = from_rgb(0, 0, 0);
			btn->style.bgcolor = from_rgb(0xC0, 0xC0, 0xC0);
			btn->style.bocolor = from_rgb(0xA0, 0xA0, 0xF0);
			btn->style.focolor = from_rgb(0x40, 0x40, 0x40);
			addWidget(uiinf, listdata->win, btn, 1);
			listdata->windowids[btn->id] = i;
		}
	}
	redrawWindow(uiinf, listdata->win);
}
static void handlePowerKey(struct uiinfo *uiinf)
{
	switch(uiinf->slidestate){
		case SLIDE_CLOSED:
			handlePowerOff(uiinf);
			break;
		case SLIDE_FULLOPEN:
		case SLIDE_HALFOPEN:
			showWindowList(uiinf);
			break;
		default:
			break;
	}
}
static void handleKeyup(struct uiinfo *uiinf, struct ui_window *win,
	int code)
{
	if(!win) return;
	win->event(win->userdata, code, UI_EVENT_KEYUP);
	if(code == KEY_POWER) handlePowerKey(uiinf);
	if(code == KEY_LEFTSHIFT || code == KEY_RIGHTSHIFT) uiinf->shift = 0;
	if(code == KEY_LEFTCTRL || code == KEY_RIGHTCTRL) uiinf->ctrl = 0;
	if(code == KEY_LEFTALT || code == KEY_RIGHTALT) uiinf->alt = 0;
}
static void handleKeydown(struct uiinfo *uiinf, struct ui_window *win,
	int code)
{
	if(!win) return;
	switch(code){
		case KEY_DOWN:
			if(win->focused && win->focused->nextfoc &&
				win->focused->nextfoc->y <= (win->height +
				win->height/2))
			{
				focus(uiinf, win, win->focused->nextfoc);
				redrawWindow(uiinf, win);
			}
			else if(win->scrolloffset < win->maxscroll){
				win->scrolloffset += 25;
				if(win->scrolloffset > win->maxscroll)
					win->scrolloffset = win->maxscroll;
				redrawWindow(uiinf, win);
			}
			else if(win->firstfocusable){
				focus(uiinf, win, win->firstfocusable);
				redrawWindow(uiinf, win);
			}
			break;
		case KEY_UP:
			if(win->focused && win->focused->prevfoc &&
				win->focused->prevfoc->y >= -win->height/2)
			{
				focus(uiinf, win, win->focused->prevfoc);
				redrawWindow(uiinf, win);
			}
			else if(win->scrolloffset > 0){
				win->scrolloffset -= 25;
				if(win->scrolloffset < 0)
					win->scrolloffset = 0;
				redrawWindow(uiinf, win);
			}
			else if(win->lastfocusable){
				focus(uiinf, win, win->lastfocusable);
				redrawWindow(uiinf, win);
			}
			break;
		case KEY_ENTER:
			if(win->focused)
				win->event(win->userdata, win->focused->id,
					UI_EVENT_CLICK);
			break;
		case KEY_LEFT:
			if(win->focused && win->focused->type == UI_FOINPUT
				&& handleInputLeft(uiinf, win->focused))
			{
				break;
			}
			if(win->focused && win->focused->leftfoc){
				win->cbx_focus_index--;
				focus(uiinf, win, win->focused->leftfoc);
				redrawWindow(uiinf, win);
			}
			break;
		case KEY_RIGHT:
			if(win->focused && win->focused->type == UI_FOINPUT
				&& handleInputRight(uiinf, win->focused))
			{
				break;
			}
			if(win->focused && win->focused->rightfoc){
				win->cbx_focus_index++;
				focus(uiinf, win, win->focused->rightfoc);
				redrawWindow(uiinf, win);
			}
			break;
		case KEY_1: keyIn(uiinf, win, 1); break;
		case KEY_2: keyIn(uiinf, win, 2); break;
		case KEY_3: keyIn(uiinf, win, 3); break;
		case KEY_4: keyIn(uiinf, win, 4); break;
		case KEY_5: keyIn(uiinf, win, 5); break;
		case KEY_6: keyIn(uiinf, win, 6); break;
		case KEY_7: keyIn(uiinf, win, 7); break;
		case KEY_8: keyIn(uiinf, win, 8); break;
		case KEY_9: keyIn(uiinf, win, 9); break;
		case KEY_0: keyIn(uiinf, win, 0); break;
		case KEY_Q: letterInput(uiinf, win, 'Q'); break;
		case KEY_W: letterInput(uiinf, win, 'W'); break;
		case KEY_E: letterInput(uiinf, win, 'E'); break;
		case KEY_R: letterInput(uiinf, win, 'R'); break;
		case KEY_T: letterInput(uiinf, win, 'T'); break;
		case KEY_Y: letterInput(uiinf, win, 'Y'); break;
		case KEY_U: letterInput(uiinf, win, 'U'); break;
		case KEY_I: letterInput(uiinf, win, 'I'); break;
		case KEY_O: letterInput(uiinf, win, 'O'); break;
		case KEY_P: letterInput(uiinf, win, 'P'); break;
		case KEY_A: letterInput(uiinf, win, 'A'); break;
		case KEY_S: letterInput(uiinf, win, 'S'); break;
		case KEY_D: letterInput(uiinf, win, 'D'); break;
		case KEY_F: letterInput(uiinf, win, 'F'); break;
		case KEY_G: letterInput(uiinf, win, 'G'); break;
		case KEY_H: letterInput(uiinf, win, 'H'); break;
		case KEY_J: letterInput(uiinf, win, 'J'); break;
		case KEY_K: letterInput(uiinf, win, 'K'); break;
		case KEY_L: letterInput(uiinf, win, 'L'); break;
		case KEY_Z: letterInput(uiinf, win, 'Z'); break;
		case KEY_X: letterInput(uiinf, win, 'X'); break;
		case KEY_C: letterInput(uiinf, win, 'C'); break;
		case KEY_V: letterInput(uiinf, win, 'V'); break;
		case KEY_B: letterInput(uiinf, win, 'B'); break;
		case KEY_N: letterInput(uiinf, win, 'N'); break;
		case KEY_M: letterInput(uiinf, win, 'M'); break;
		case KEY_LEFTSHIFT:
		case KEY_RIGHTSHIFT:
			uiinf->shift = 1; break;
		case KEY_LEFTCTRL:
		case KEY_RIGHTCTRL:
			uiinf->ctrl = 1; break;
		case KEY_LEFTALT:
		case KEY_RIGHTALT:
			uiinf->alt = 1; break;
		case KEY_NUMERIC_STAR: keyIn(uiinf, win, 10); break;
		case KEY_NUMERIC_POUND: keyIn(uiinf, win, 11); break;
		case KEY_BACKSPACE: {
			char *text;
			int i;
			unsigned char *cursorpos, *scrolloffset;
			uiinf->curkey = -1;
			if(!win->focused || win->focused->type !=
				UI_FOINPUT)
			{
				win->event(win->userdata, 0, UI_EVENT_EXIT);
				break;
			}
			scrolloffset = ((unsigned char*)win->focused->data);
			cursorpos = ((unsigned char*)win->focused->data)+1;
			if(*cursorpos == 0){
				win->event(win->userdata, 0, UI_EVENT_EXIT);
				break;
			}
			text = ((char*)win->focused->data)+2;
			for(i = *cursorpos; i < 255; i++){
				text[i-1] = text[i];
			}
			text[255] = 0;
			if(*scrolloffset > 0) (*scrolloffset)--;
			(*cursorpos)--;
			redrawWidget(uiinf, win, win->focused);
			refreshScreen(uiinf->fbinf);
			break;
		}
		case KEY_TAB:
			if(uiinf->alt) showWindowList(uiinf);
			break;
	}
	win->event(win->userdata, code, UI_EVENT_KEYDOWN);
}
void uiLoop(struct uiinfo *uiinf, int *custom_fds, int *ncustom_fds,
	fd_callback custom_callback, void *userdata)
{
	int i, mfds = 0, resuspend, suspend_on_idle = 0;
	struct timeval timeout;
	struct transition *trans;
	fd_set readfds;
	while(1){
		int res;
		resuspend = 0;
		if(suspend_on_idle && !uiinf->on && !uiinf->turning_on){
			/* The wakeup time should be any longer - if it does,
			 * power-on events might get ignored.
			 */
			timeout.tv_sec = 3;
			timeout.tv_usec = 500000;
		}
		else if(suspend_on_idle) suspend_on_idle = 0;
		else if(uiinf->transitions){
			timeout.tv_sec = 0;
			timeout.tv_usec = 0;
		}
		else {
			timeout.tv_sec = timeout_getSeconds(uiinf->timeouts);
			if(timeout.tv_sec > 1){
				timeout.tv_sec = 1;
				timeout.tv_usec = 0;
			}
			else timeout.tv_usec =
				timeout_getUsecs(uiinf->timeouts);
		}
		/*fprintf(stderr, "DEBUG: timeout: %lu seconds, %lu usecs\n",
			timeout.tv_sec, timeout.tv_usec);*/
		FD_ZERO(&readfds);
		for(i = 0; i < uiinf->nfds; i++){
			FD_SET(uiinf->inpfds[i], &readfds);
			if(uiinf->inpfds[i] >= mfds)
				mfds = uiinf->inpfds[i]+1;
		}
		for(i = 0; i < *ncustom_fds; i++){
			FD_SET(custom_fds[i], &readfds);
			if(custom_fds[i] >= mfds)
				mfds = custom_fds[i]+1;
		}
		res = select(mfds, &readfds, NULL, NULL,
			(uiinf->on || suspend_on_idle) ? &timeout : NULL);
		if(res < 0){
			perror("select");
			uicleanup(uiinf);
			exit(1);
		}
		if(uiinf->on){
			for(trans = uiinf->transitions; trans;
				trans = trans->next)
			{
				stepTransition(uiinf, trans);
			}
		}
		if(res == 0){
			if(suspend_on_idle) handlePowerOff(uiinf);
			else if(uiinf->curwindow &&
				!uiinf->curwindow->hidetoppanel)
			{
				showTopPanel(uiinf);
				refreshScreen(uiinf->fbinf);
			}
			uiinf->curkey = -1;
			continue;
		}
		handleTimeouts(&(uiinf->timeouts));
		for(i = 0; i < uiinf->nfds; i++){
			if(FD_ISSET(uiinf->inpfds[i], &readfds)){
				struct input_event event;
				read(uiinf->inpfds[i], &event,
					sizeof(struct input_event));
				if(!uiinf->on && event.type == EV_KEY){
					suspend_on_idle = 0;
					handlePowerOn(uiinf);
					/* uiinf->turning_on is used to prevent
					 * from turning off directly after
					 * the phone was turned on using the
					 * power button, because it turns off
					 * when the button is released but
					 * turns on when it is pressed.
					 */
					uiinf->turning_on = 1;
				}
				else if(!uiinf->on && !uiinf->turning_on){
					suspend_on_idle = 1;
				}
				if(event.type == EV_KEY && event.value == 0 &&
					uiinf->turning_on)
				{
					uiinf->turning_on = 0;
					if(event.code != KEY_SLIDE) continue;
				}
				if(event.type == EV_KEY &&
					event.code == KEY_SLIDE &&
					(i == 1 || i == 2) && event.value == 0)
				{
					uiinf->slidestate = SLIDE_HALFOPEN;
				}
				else if(event.type == EV_KEY &&
					event.code == KEY_SLIDE && i == 1 &&
					event.value == 1)
				{
					uiinf->slidestate = SLIDE_CLOSED;
					handlePowerOff(uiinf);
				}
				else if(event.type == EV_SW &&
					event.code == KEY_SLIDE && i == 2 &&
					event.value == 1)
				{
					uiinf->slidestate = SLIDE_FULLOPEN;
					handleSlideFullopen(uiinf);
				}
				if(event.type == EV_KEY &&
					event.value == 1) /* Key Down */
				{
					if(event.code == KEY_POWER &&
						i == 4)
					{
						event.code = KEY_BACKSPACE;
					}
					if(event.code == KEY_OK){
						event.code = KEY_ENTER;
					}
					else if(event.code == KEY_F6){
						event.code = KEY_MENU;
					}
					else if(event.code == KEY_F7){
						event.code = KEY_BACK;
					}
					handleKeydown(uiinf,
						uiinf->curwindow,
						event.code);
				}
				else if(event.type == EV_KEY &&
					event.value == 0) /* Key Up */
				{
					if(event.code == KEY_POWER &&
						i == 4)
					{
						event.code = KEY_BACKSPACE;
					}
					if(event.code == KEY_OK){
						event.code = KEY_ENTER;
					}
					handleKeyup(uiinf,
						uiinf->curwindow,
						event.code);
				}
			}
		}
		for(i = 0; i < *ncustom_fds; i++){
			if(FD_ISSET(custom_fds[i], &readfds)){
				resuspend = 1;
				custom_callback(userdata, i);
			}
		}
		if(!uiinf->on && !uiinf->turning_on && resuspend){
			suspend_on_idle = 1;
		}
	}
}
void makeUnfocusable(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget)
{
	widget->focusable = 0;
	if(win->focused == widget) win->focused =
		win->focused->nextfoc ? win->focused->nextfoc :
			win->focused->prevfoc;
	if(!widget->cbx){
		if(widget->prevfoc){
			widget->prevfoc->nextfoc = widget->nextfoc;
		}
		else {
			win->firstfocusable = widget->nextfoc;
		}
		if(widget->nextfoc){
			widget->nextfoc->prevfoc = widget->prevfoc;
		}
		else {
			win->lastfocusable = widget->prevfoc;
		}
	}
	else {
		if(widget->leftfoc){
			widget->leftfoc->rightfoc = widget->rightfoc;
		}
		else {
			((struct ui_cbx_info*)widget->cbx->data)->firstfoc=
				widget->rightfoc;
		}
		if(widget->cbx && widget->rightfoc){
			widget->rightfoc->leftfoc = widget->leftfoc;
		}
		else {
			((struct ui_cbx_info*)widget->cbx->data)->lastfoc =
				widget->leftfoc;
		}
	}
}

void makeFocusable(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget)
{
	if(!widget->cbx){
		if(!win->firstfocusable){
			redrawWindow(uiinf, win);
			focus(uiinf, win, widget);
		}
		/*	Find the next focusable widget -
			it has the focusable flag set. */
		for(widget->nextfoc = widget;
			widget->nextfoc && !widget->nextfoc->focusable;
			widget->nextfoc = widget->nextfoc->next) {}
		if(widget->nextfoc){
			widget->prevfoc = widget->nextfoc->prevfoc;
			widget->nextfoc->prevfoc = widget;
		}
		else {
			win->lastfocusable = widget;
			/*	Find the previous focusable widget -
				no nextfoc has been found. */
			for(widget->prevfoc = widget;
				widget->prevfoc &&
				!widget->prevfoc->focusable;
				widget->prevfoc = widget->prevfoc->prev) {}
			if(!widget->prevfoc){
				/* widget is the first focusable widget */
				win->firstfocusable = widget;
			}
		}
		if(!widget->prevfoc){
			/* widget is the first focusable widget */
			win->firstfocusable = widget;
		}
	}
	else {
		struct ui_cbx_info *cbxi = widget->cbx->data;
		if(!cbxi->firstfoc){
			redrawWindow(uiinf, win);
			focus(uiinf, win, widget);
		}
		/*	Find the next focusable widget -
			it has the focusable flag set. */
		for(widget->rightfoc = widget;
			widget->rightfoc && !widget->rightfoc->focusable;
			widget->rightfoc = widget->rightfoc->right) {}
		if(widget->rightfoc){
			widget->leftfoc = widget->rightfoc->leftfoc;
			widget->rightfoc->leftfoc = widget;
		}
		else {
			cbxi->lastfoc = widget;
			/*	Find the previous focusable widget -
				no rightfoc has been found. */
			for(widget->leftfoc = widget;
				widget->leftfoc &&
				!widget->leftfoc->focusable;
				widget->leftfoc = widget->leftfoc->left) {}
		}
		if(!widget->leftfoc){
			/* widget is the first focusable widget */
			cbxi->firstfoc = widget;
		}
	}
	widget->focusable = 1;
}
struct uiinfo *getui()
{
	struct uiinfo *uiinf;
	int i;
	DIR* input_dir;
	struct dirent* input_dirent;
	uiinf = malloc(sizeof(struct uiinfo));
	uiinf->fbinf = getfb(GR_QCOM_BACKEND);
	if(uiinf->fbinf == NULL){
		fprintf(stderr, "Failed to create graphics backend.\n");
		return NULL;
	}
	uiinf->on = 1;
	uiinf->turning_on = 0;
	uiinf->slidestate = SLIDE_CLOSED;
	for(i = 0; i < MAX_WINDOWS; i++){
		uiinf->windows[i] = NULL;
	}
	uiinf->nextwindowid = 0;
	uiinf->curkey = -1;
	uiinf->inptype = INPTYPE_ALPHA_LOWER;
	uiinf->timeouts = NULL;
	uiinf->transitions = NULL;
	uiinf->shift = 0;
	uiinf->ctrl = 0;
	uiinf->alt = 0;
	input_dir = opendir("/dev/input");
	if(!input_dir){
		fprintf(stderr, "opendir /dev/input failed");
		exit(2);
	}
	for(uiinf->nfds = 0; uiinf->nfds < MAX_INPUTS &&
		(input_dirent = readdir(input_dir)); uiinf->nfds++)
	{
		int id;
		if(0 != strncmp(input_dirent->d_name, "event", 5)){
			uiinf->nfds--;
			continue;
		}
		id = strtol(input_dirent->d_name + 5, NULL, 10);
		uiinf->inpfds[id] = openat(dirfd(input_dir),
			input_dirent->d_name, O_RDONLY|O_CLOEXEC);
		if(uiinf->inpfds[id] == -1){
			perror(input_dirent->d_name);
		}
	}
	font_init();
	uiinf->slidestate = SLIDE_CLOSED;
	uiinf->windowlist = NULL;
	handlePowerOn(uiinf);
	return uiinf;
}

void uicleanup(struct uiinfo *uiinf)
{
	fbcleanup(uiinf->fbinf);
}
