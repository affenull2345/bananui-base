EXECS = bananui testclient bananui-homescreen bananui-files colorgrid \
	bananui-login bananui-shutdown flashlightctl vibratorctl terminal-stub \
	about-bananui
HELPERS = bananui-bg
INITSCRIPTS = initscripts/bananui
UISOURCES = gr/gr.c gr/gr_qcom.c gr/extra.c ui.c uiserv.c icon.c ini.c font.c
UIOBJECTS = $(UISOURCES:.c=.o)
TESTSOURCES = testclient.c
TESTOBJECTS = $(TESTSOURCES:.c=.o)
HOMESCRSOURCES = homescreen.c
HOMESCROBJECTS = $(HOMESCRSOURCES:.c=.o)
BROWSERSOURCES = browser.c
BROWSEROBJECTS = $(BROWSERSOURCES:.c=.o)
ABOUTSOURCES = about.c
ABOUTOBJECTS = $(ABOUTSOURCES:.c=.o)
TERMSTUBSOURCES = terminal-stub.c
TERMSTUBOBJECTS = $(TERMSTUBSOURCES:.c=.o)
COLORGSOURCES = colorgrid.c
COLORGOBJECTS = $(COLORGSOURCES:.c=.o)
LOGINSOURCES = login.c
LOGINOBJECTS = $(LOGINSOURCES:.c=.o)
SHUTDOWNSOURCES = shutdown.c
SHUTDOWNOBJECTS = $(SHUTDOWNSOURCES:.c=.o)
FLASHLIGHTSOURCES = flashlightctl.c
FLASHLIGHTOBJECTS = $(FLASHLIGHTSOURCES:.c=.o)
VIBRATORSOURCES = vibratorctl.c
VIBRATOROBJECTS = $(VIBRATORSOURCES:.c=.o)
VERSION = $(shell cat /tmp/bananian-version)
CFLAGS = -g -Wall -Wno-unused-function -DHAVE_DEBUG -DVERSION="\"$(VERSION)\"" \
	-DGR_QCOM $(shell GraphicsMagickWand-config --cppflags)

all: $(EXECS)

bananui: $(UIOBJECTS)
	$(CC) -o $@ $(UIOBJECTS) $(CFLAGS) \
		$(shell GraphicsMagickWand-config --ldflags --libs)

install:
	install $(EXECS) $(HELPERS) $(DESTDIR)/usr/bin
	install $(INITSCRIPTS) $(DESTDIR)/etc/init.d
	#install defconfig/* $(DESTDIR)/etc/bananui
	install 99-bananui-no-power-keys.conf \
		$(DESTDIR)/etc/systemd/logind.conf.d/
	install desktop/*.desktop $(DESTDIR)/usr/share/applications
	for size in 32x32 48x48 64x64 128x128 256x256 512x512; do \
		install icons/banana_$${size}.png \
		$(DESTDIR)/usr/share/icons/hicolor/$${size}/apps/banana.png; \
	done

testclient: $(TESTOBJECTS)
	$(CC) -o $@ $(TESTOBJECTS) $(CFLAGS) $(LDFLAGS)

bananui-homescreen: $(HOMESCROBJECTS) ini.o
	$(CC) -o $@ $(HOMESCROBJECTS) $(CFLAGS) $(LDFLAGS) ini.o -lbananui

bananui-files: $(BROWSEROBJECTS)
	$(CC) -o $@ $(BROWSEROBJECTS) $(CFLAGS) $(LDFLAGS)

terminal-stub: $(TERMSTUBOBJECTS)
	$(CC) -o $@ $(TERMSTUBOBJECTS) $(CFLAGS) $(LDFLAGS) -lbananui

colorgrid: $(COLORGOBJECTS)
	$(CC) -o $@ $(COLORGOBJECTS) $(CFLAGS) $(LDFLAGS)

about-bananui: $(ABOUTOBJECTS)
	$(CC) -o $@ $(ABOUTOBJECTS) $(CFLAGS) $(LDFLAGS) -lbananui

bananui-login: $(LOGINOBJECTS)
	$(CC) -o $@ $(LOGINOBJECTS) $(CFLAGS) $(LDFLAGS) -lpam

bananui-shutdown: $(SHUTDOWNOBJECTS)
	$(CC) -o $@ $(SHUTDOWNOBJECTS) $(CFLAGS) $(LDFLAGS)

flashlightctl: $(FLASHLIGHTOBJECTS)
	$(CC) -o $@ $(FLASHLIGHTOBJECTS) $(CFLAGS) $(LDFLAGS)

vibratorctl: $(VIBRATOROBJECTS)
	$(CC) -o $@ $(VIBRATOROBJECTS) $(CFLAGS) $(LDFLAGS)

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $< -o $@

ifneq (clean, $(MAKECMDGOALS))
-include deps.mk
endif

deps.mk: $(UISOURCES)
	$(CC) $(CFLAGS) -MM $^ > $@

clean:
	rm -f deps.mk *.o gr/*.o $(EXECS)
