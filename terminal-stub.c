/*
        Terminal stub for bananui
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <linux/input-event-codes.h>
#include <bananui/bananui.h>

static const char termstr[] =
"This application was meant to be run in a terminal, but no terminal is available. ";

int eventCallback(bEventType type, bEventData *evinfo)
{
	if(type == BEV_EXIT
		|| (type == BEV_KEYDOWN && evinfo->val == KEY_ENTER))
	{
		return 0;
	}
	return -1;
}

int main()
{
	bWindow *wnd;
	wnd = bCreateWnd();
	if(!wnd) return 1;
	bSetWindowTitle(wnd, "@utilities-terminal@Terminal (stub)");
	bCreateLabel(wnd, termstr, TA_LEFT, NO_WIDGET);
	bSetSoftkeys(wnd, "", "OK", "");
	bSetEventCallback(wnd, eventCallback);

	return bEventLoop(wnd);
}
