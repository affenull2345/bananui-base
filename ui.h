/*
	The BananUI user interface. For the socket and command implementation,
	see uiserv.c.
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _UI_H_
#define _UI_H_
#define MAX_WINDOWS 128
#define MAX_WIDGETS 256
#define MAX_INPUTS 16
#define TITLE_SIZE 128
#define ICON_THEME_NAME_SIZE 50
#define SK_LABEL_SIZE 16
#include <linux/input.h>
#include <time.h>
#include <stdint.h>
#include "gr/gr.h"

enum ui_widget_type { UI_LABEL, UI_BUTTON, UI_INPUT, UI_FOBUTTON,
	UI_FOINPUT, UI_CBX, UI_IMAGE, UI_ICON };
enum ui_text_align { UI_TA_LEFT, UI_TA_CENTER, UI_TA_RIGHT };
enum ui_input_type { UI_INP_NORMAL, UI_INP_PASSWORD };
enum ui_event { UI_EVENT_CLICK, UI_EVENT_KEYUP, UI_EVENT_KEYDOWN,
	UI_EVENT_EXIT, UI_EVENT_FOCUS };
enum slide_state { SLIDE_CLOSED, SLIDE_HALFOPEN, SLIDE_FULLOPEN };
enum transition_style { TRANS_NONE, TRANS_POPUP, TRANS_POPDOWN, TRANS_CROSS };

typedef void (*fd_callback)(void*, int);

#define from_rgb(r, g, b) ((r) << 24 | (g) << 16 | (b) << 8 | 255)
#define from_rgba(r, g, b, a) ((r) << 24 | (g) << 16 | (b) << 8 | (a))

static int get_red(unsigned int color)
{
	return color >> 24;
}
static int get_green(unsigned int color)
{
	return color >> 16 & 255;
}
static int get_blue(unsigned int color)
{
	return color >> 8 & 255;
}
static int get_alpha(unsigned int color)
{
	return color & 255;
}

struct ui_style {
	unsigned int fgcolor;
	unsigned int bgcolor;
	unsigned int focolor;
	unsigned int bocolor;
};

struct icon_widget_data {
	int height, width;
	char name[256];
};

struct ui_widget {
	enum ui_widget_type type;
	union {
		enum ui_text_align textalign;
		enum ui_input_type inputtype;
	};
	int id, focusable, freedata;
	int x, y, rightend, height;
	void *data;
	struct ui_widget *next;
	struct ui_widget *prev;
	struct ui_widget *right;
	struct ui_widget *left;
	struct ui_widget *nextfoc;
	struct ui_widget *prevfoc;
	struct ui_widget *leftfoc;
	struct ui_widget *rightfoc;
	struct ui_widget *cbx;
	struct ui_style style;
};

typedef void (*event_callback)(void*, int, enum ui_event);
typedef void (*timeout_callback)(void*);

struct ui_window {
	struct ui_widget* widgets[MAX_WIDGETS];
	struct ui_window *parent;
	struct ui_widget *firstfocusable;
	struct ui_widget *lastfocusable;
	struct ui_widget *firstwidget;
	struct ui_widget *lastwidget;
	struct ui_widget *focused;
	int skbgr, skbgg, skbgb, skbga;
	int skfgr, skfgg, skfgb;
	int bgr, bgg, bgb;
	int tpbgr, tpbgg, tpbgb;
	int tpfgr, tpfgg, tpfgb;
	char title[TITLE_SIZE], icontheme[ICON_THEME_NAME_SIZE];
	char current_skl[SK_LABEL_SIZE];
	char current_skc[SK_LABEL_SIZE];
	char current_skr[SK_LABEL_SIZE];
	uint32_t *buffer;
	uint32_t *oldbuffer;
	int nextwidgetid, id, cbx_focus_index;
	int scrolloffset, height, actual_yoffset, maxscroll;
	event_callback event;
	void *userdata;
	unsigned int hidetoppanel	: 1;
	unsigned int hideskpanel	: 1;
};

enum input_type { INPTYPE_NUMERIC, INPTYPE_ALPHA_LOWER, INPTYPE_ALPHA_UPPER };

struct timeout {
	struct timeout *next;
	struct timeval end_time;
	timeout_callback cb;
	void *userdata;
};

struct uiinfo {
	struct fbinfo *fbinf;
	struct ui_window* windows[MAX_WINDOWS];
	struct ui_window *curwindow;
	struct ui_window *windowlist;
	struct timeout *timeouts;
	struct transition *transitions;
	int nextwindowid, nfds, inpfds[MAX_INPUTS], on, turning_on;
	enum slide_state slidestate;
	enum input_type inptype;
	int curkey, keyindex;
	unsigned int shift	: 1;
	unsigned int ctrl	: 1;
	unsigned int alt	: 1;
};

struct ui_cbx_info {
	struct ui_widget *first;
	struct ui_widget *last;
	struct ui_widget *firstfoc;
	struct ui_widget *lastfoc;
	int numwidgets;
};

void redrawWidget(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget);
void focus(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget);
struct ui_window *createWindow(struct uiinfo *uiinf);
void destroyWindow(struct uiinfo *uiinf, struct ui_window *win);
void clearWindow(struct uiinfo *uiinf, struct ui_window *win);
void redrawWindowTransition(struct uiinfo *uiinf, struct ui_window *win,
	enum transition_style trans, suseconds_t speed);
void redrawWindow(struct uiinfo *uiinf, struct ui_window *win);
void addWidget(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget, int focusable);
void uiLoop(struct uiinfo *uiinf, int *custom_fds, int *ncustom_fds,
	fd_callback custom_callback, void *userdata);
void makeFocusable(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget);
void makeUnfocusable(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *widget);
void addWidgetToCbx(struct uiinfo *uiinf, struct ui_window *win,
	struct ui_widget *cbx, struct ui_widget *widget, int focusable);
struct uiinfo *getui();
void uicleanup(struct uiinfo *uiinf);
void handlePowerOn(struct uiinfo *uiinf);
void handlePowerOff(struct uiinfo *uiinf);

#endif
