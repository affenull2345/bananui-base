/*
	Graphics library to handle qualcomm framebuffers.
	Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _GR_QCOM_H_
#define _GR_QCOM_H_
#include <linux/fb.h>
#include <stdint.h>
#include "msm_mdp.h"
#include "ion.h"

struct qcom_fbinfo {
	uint16_t *framebuffer, *ion_mem;
	struct fb_fix_screeninfo *finfo;
	struct fb_var_screeninfo *vinfo;
	struct mdp_overlay *overlay;
	int fd, memid, iondev_fd;
	ion_user_handle_t ion_handle;
};

void qcom_fbcpy(struct qcom_fbinfo *dst, const uint32_t *src, int yoffset,
	int xoffset, int yres, int xres);
int qcom_getxres(struct qcom_fbinfo *info);
int qcom_getyres(struct qcom_fbinfo *info);
void qcom_fbcleanup(struct qcom_fbinfo *info);
void qcom_refreshScreen(struct qcom_fbinfo *info);
void qcom_blankScreen(struct qcom_fbinfo *info, int blank);
void qcom_setPixel(struct qcom_fbinfo *info, int y, int x, int r, int g, int b,
	int a);
void qcom_drawRect(struct qcom_fbinfo *info, int y, int x, int h, int w, int r,
	int g, int b, int a);
struct qcom_fbinfo *qcom_getfb();

#endif
