/*
	Forward framebuffer requests to the backend.
	Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _GR_H_
#define _GR_H_

#include <stdint.h>

enum gr_backend {
#ifdef GR_QCOM
	GR_QCOM_BACKEND,
#endif
#ifdef GR_FBDEV
	GR_FBDEV_BACKEND,
#endif
	GR_EMULATED_BACKEND,
	GR_DUMMY_BACKEND
};

struct emulated_fbinfo {
	uint32_t *buffer;
	int yres, xres;
	enum gr_backend saved_backend;
};

struct fbinfo {
	enum gr_backend backend;
	union {
#ifdef GR_QCOM
		struct qcom_fbinfo *qcom_info;
#endif
#ifdef GR_FBDEV
		struct fbdev_fbinfo *fbdev_info;
#endif
		struct emulated_fbinfo *emulated_info;
		void *generic_info;
	};
};

int getxres(struct fbinfo *info);
int getyres(struct fbinfo *info);
void fbcpy(struct fbinfo *dst, const uint32_t *src, int yoffset, int xoffset,
	int yres, int xres);
void fbcleanup(struct fbinfo *info);
void emulateFramebuffer(struct fbinfo *info, uint32_t **buffer, int yres,
	int xres);
void restoreFramebuffer(struct fbinfo *info, uint32_t **buffer);
void refreshScreen(struct fbinfo *info);
void blankScreen(struct fbinfo *info, int blank);
void setPixel(struct fbinfo *info, int y, int x, int r, int g, int b,
	int a);
void drawRect(struct fbinfo *info, int y, int x, int h, int w, int r,
	int g, int b, int a);
struct fbinfo *getfb(enum gr_backend betype);

#endif
