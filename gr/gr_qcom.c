/*
	Graphics library to handle qualcomm framebuffers.
	Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <linux/fb.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gr_qcom.h"
#include "ion.h"
#include "msm_ion.h"
#define FBDEV "/dev/fb0"


void qcom_fbcleanup(struct qcom_fbinfo *info)
{
	munmap(info->ion_mem, info->finfo->smem_len);
	if(ioctl(info->iondev_fd, ION_IOC_FREE, &info->ion_handle) < 0){
		perror("ioctl ION_IOC_FREE");
	}
	close(info->fd);
	free(info->framebuffer);
	free(info->finfo);
	free(info->vinfo);
	free(info->overlay);
	free(info);
}
void qcom_refreshScreen(struct qcom_fbinfo *info)
{
	struct mdp_display_commit commit;
	struct msmfb_overlay_data overlay_data;
	/*if(info->vinfo->yoffset > 0){
		info->vinfo->yoffset = 0;
		memcpy(info->framebuffer + (info->finfo->smem_len / 2),
			info->framebuffer, info->finfo->smem_len / 2);
		info->framebuffer += (info->finfo->smem_len / 2);
	}
	else {
		info->vinfo->yoffset = info->vinfo->yres;
		memcpy(info->framebuffer - (info->finfo->smem_len / 2),
			info->framebuffer, info->finfo->smem_len / 2);
		info->framebuffer -= (info->finfo->smem_len / 2);
	}*/
	memset(&overlay_data, 0, sizeof(struct msmfb_overlay_data));
	overlay_data.data.flags = 0;
	overlay_data.data.offset = 0;
	overlay_data.data.memory_id = info->memid;
	overlay_data.id = info->overlay->id;
	if(ioctl(info->fd, MSMFB_OVERLAY_PLAY, &overlay_data) < 0){
		perror("ioctl MSMFB_OVERLAY_PLAY");
	}
	memset(&commit, 0, sizeof(struct mdp_display_commit));
	commit.flags = MDP_DISPLAY_COMMIT_OVERLAY;
	commit.wait_for_finish = 1;
	memcpy(info->ion_mem, info->framebuffer, info->finfo->line_length *
		info->vinfo->yres);
	if(ioctl(info->fd, MSMFB_DISPLAY_COMMIT, &commit) < 0){
		perror("ioctl MSMFB_DISPLAY_COMMIT");
	}
	/*if(ioctl(info->fd, FBIOPUT_VSCREENINFO, info->vinfo)){
		perror("ioctl FBIOPUT_VSCREENINFO");
	}*/
}
void qcom_blankScreen(struct qcom_fbinfo *info, int blank)
{
	if(blank){
		if(ioctl(info->fd, FBIOBLANK, FB_BLANK_POWERDOWN)){
			perror("ioctl FBIOBLANK FB_BLANK_POWERDOWN");
		}
	}
	else {
		if(ioctl(info->fd, FBIOBLANK, FB_BLANK_UNBLANK)){
			perror("ioctl FBIOBLANK FB_BLANK_UNBLANK");
		}
		info->overlay->id = MSMFB_NEW_REQUEST;
		if(ioctl(info->fd, MSMFB_OVERLAY_SET, info->overlay)){
			perror("ioctl MSMFB_OVERLAY_SET");
		}
		qcom_refreshScreen(info);
	}
}
void qcom_setPixel(struct qcom_fbinfo *info, int y, int x, int r, int g,
	int b, int a)
{
	int val;
	uint16_t *fb;
	if(y > info->vinfo->yres || x > info->vinfo->xres) return;
	r >>= 3;
	g >>= 2;
	b >>= 3;
	fb = info->framebuffer;
	if(a != 255){
		uint16_t oldpixel;
		int oldr, oldg, oldb;
		oldpixel = fb[y*info->finfo->line_length/2 + x];
		oldr = oldpixel >> 11;
		oldg = (oldpixel >> 5) & 63;
		oldb = oldpixel & 31;
		r = (r * a / 255) + (oldr * (255-a) / 255);
		g = (g * a / 255) + (oldg * (255-a) / 255);
		b = (b * a / 255) + (oldb * (255-a) / 255);
	}
	fb[y*info->finfo->line_length/2 + x] = ((r & 31) << 11) |
		((g & 63) << 5) | (b & 31);
}

void qcom_drawRect(struct qcom_fbinfo *info, int y, int x, int h, int w,
	int r, int g, int b, int a)
{
	int i, j;
	for(i = y; i < y + h; i++){
		if(i + h < 0) break;
		if(i > 0 && i >= info->vinfo->yres) break;
		for(j = x; j < x + w; j++){
			if(j + w < 0) break;
			if(j > 0 && j >= info->vinfo->xres) break;
			qcom_setPixel(info, i, j, r, g, b, a);
		}
	}
}
/*void rainbow(struct qcom_fbinfo *info)
{
	int r=0, g=0, b=0, i=0;
	for(; r < 7; r++, i++){
		drawRect(info, i*5, 10, 5, 100, r, g, b);
	}
	for(; g < 7; g++, i++){
		drawRect(info, i*5, 10, 5, 100, r, g, b);
	}
	for(; r > 0; r--, i++){
		drawRect(info, i*5, 10, 5, 100, r, g, b);
	}
	for(; b < 3; b++, i++){
		drawRect(info, i*5, 10, 5, 100, r, g, b);
	}
	for(; g > 0; g--, i++){
		drawRect(info, i*5, 10, 5, 100, r, g, b);
	}
	for(; r < 7; r++, i++){
		drawRect(info, i*5, 10, 5, 100, r, g, b);
	}
	for(; r > 0; r--, b = b ? b-1 : 0, i++){
		drawRect(info, i*5, 10, 5, 100, r, g, b);
	}
	refreshScreen(info);
}

int main()
{*/
void qcom_fbcpy(struct qcom_fbinfo *dst, const uint32_t *src, int yoffset,
	int xoffset, int yres, int xres)
{
	int i, j, srcindex, dstindex;
	dstindex = dst->finfo->line_length/2 * yoffset + xoffset;
	for(i = 0, srcindex = 0; i < yres && (yoffset + i) <
		dst->vinfo->yres;
		i++, dstindex += (dst->finfo->line_length/2 -
			(j+xoffset)) + xoffset)
	{
		for(j = 0; j < xres; j++, srcindex++, dstindex++)
		{
			uint32_t pix8888;
			int red, green, blue;
			if((xoffset + j) >= dst->vinfo->xres){
				srcindex += (xres - j);
				break;
			}
			pix8888 = src[srcindex];
			red = (pix8888 >> 24) >> 3;
			green = ((pix8888 >> 16) & 0xFF) >> 2;
			blue = ((pix8888 >> 8) & 0xFF) >> 3;
			/*int alpha = pix8888 & 0xFF;*/
			dst->framebuffer[dstindex] =
				(red << 11) | (green << 5) | blue;
		}
	}
}
int qcom_getxres(struct qcom_fbinfo *info){
	return info->vinfo->xres;
}
int qcom_getyres(struct qcom_fbinfo *info){
	return info->vinfo->yres;
}
struct qcom_fbinfo *qcom_getfb(){
	int fd, iondev_fd;
	uint16_t *framebuffer = (uint16_t*)-1;
	struct fb_fix_screeninfo *finfo;
	struct fb_var_screeninfo *vinfo;
	struct qcom_fbinfo *info;
	/*struct msmfb_overlay_data overlay_data;*/
	struct mdp_overlay *overlay;
	/*struct mdp_display_commit commit;*/
	struct ion_fd_data ion_fd;
	struct ion_allocation_data ion_alloc;

	finfo = malloc(sizeof(struct fb_fix_screeninfo));
	vinfo = malloc(sizeof(struct fb_var_screeninfo));
	info = malloc(sizeof(struct qcom_fbinfo));
	fd = open(FBDEV, O_RDWR|O_CLOEXEC);
	if(fd < 0){
		perror(FBDEV);
		return NULL;
	}
	if(ioctl(fd, FBIOGET_FSCREENINFO, finfo) < 0){
		perror("ioctl FBIOGET_FSCREENINFO");
		return NULL;
	}
	if(ioctl(fd, FBIOGET_VSCREENINFO, vinfo) < 0){
		perror("ioctl FBIOGET_VSCREENINFO");
		return NULL;
	}
	printf("%dx%d\n", vinfo->xres, vinfo->yres);
	ion_alloc.flags = 0;
	ion_alloc.len = finfo->line_length * vinfo->yres;
	ion_alloc.align = sysconf(_SC_PAGESIZE);
	ion_alloc.heap_id_mask = ION_HEAP(ION_IOMMU_HEAP_ID) |
		ION_HEAP(21); /* ION_SYSTEM_CONTIG_HEAP_ID */
	iondev_fd = open("/dev/ion", O_RDWR|O_DSYNC|O_CLOEXEC);
	info->iondev_fd = iondev_fd;
	if(iondev_fd < 0){
		perror("/dev/ion");
		return NULL;
	}
	if(ioctl(iondev_fd, ION_IOC_ALLOC,  &ion_alloc) < 0){
		perror("ioctl ION_IOC_ALLOC");
		return NULL;
	}
	ion_fd.handle = ion_alloc.handle;
	info->ion_handle = ion_fd.handle;
	if(ioctl(iondev_fd, ION_IOC_MAP, &ion_fd) < 0){
		perror("ioctl ION_IOC_MAP");
		if(ioctl(iondev_fd, ION_IOC_FREE, &ion_fd.handle) < 0){
			perror("ioctl ION_IOC_FREE");
		}
		return NULL;
	}
	framebuffer = mmap(NULL, finfo->line_length * vinfo->yres,
		PROT_READ | PROT_WRITE,
		MAP_SHARED, ion_fd.fd, 0);
	if(framebuffer == (uint16_t*)-1){
		perror("mmap");
		return NULL;
	}
	overlay = malloc(sizeof(struct mdp_overlay));
	memset(overlay, 0, sizeof(struct mdp_overlay));
	overlay->src.width = 256;
	overlay->src.height = vinfo->yres;
	overlay->src.format = MDP_RGB_565;
	overlay->src_rect.w = vinfo->xres;
	overlay->src_rect.h = vinfo->yres;
	overlay->dst_rect.w = vinfo->xres;
	overlay->dst_rect.h = vinfo->yres;
	overlay->alpha = 0xFF;
	overlay->transp_mask = MDP_TRANSP_NOP;
	overlay->id = MSMFB_NEW_REQUEST;
	if(ioctl(fd, MSMFB_OVERLAY_SET, overlay) < 0){
		perror("ioctl MSMFB_OVERLAY_SET");
	}
	/*memset(&overlay_data, 0, sizeof(struct msmfb_overlay_data));
	overlay_data.data.flags = 0;
	overlay_data.data.offset = 0;
	overlay_data.data.memory_id = ion_fd.fd;
	overlay_data.id = overlay->id;*/
	info->overlay = overlay;
	info->memid = ion_fd.fd;
	/*if(ioctl(fd, MSMFB_OVERLAY_PLAY, &overlay_data) < 0){
		perror("ioctl MSMFB_OVERLAY_PLAY");
		return NULL;
	}
	memset(&commit, 0, sizeof(struct mdp_display_commit));
	commit.flags = MDP_DISPLAY_COMMIT_OVERLAY;
	commit.wait_for_finish = 1;
	if(ioctl(fd, MSMFB_DISPLAY_COMMIT, &commit) < 0){
		perror("ioctl MSMFB_DISPLAY_COMMIT");
	}*/
	info->finfo = finfo;
	info->vinfo = vinfo;
	info->framebuffer = malloc(finfo->line_length * vinfo->yres);
	info->ion_mem = framebuffer;
	info->fd = fd;
	return info;
}
