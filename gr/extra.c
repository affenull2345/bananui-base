/*
	Graphics for the Bananui user interface - buffer cross-fade
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "extra.h"

void blend_buffers(uint32_t *buf1, uint32_t *buf2, enum blend_style style,
	int yres, int xres, int percentage2, uint32_t *outbuf)
{
	int i = 0, percentage1 = 100-percentage2;
	switch(style){
		case BLEND_STYLE_CROSSFADE:
			for(i = 0; i < (yres * xres); i++){
				int red1, green1, blue1, red, green, blue;
				red1 = (buf1[i] >> 24) * percentage1 / 100;
				green1 = ((buf1[i] >> 16) & 0xFF) *
					percentage1 / 100;
				blue1 = ((buf1[i] >> 8) & 0xFF) * percentage1 /
					100;
				red = (buf2[i] >> 24) * percentage2 / 100 +
					red1;
				green = ((buf2[i] >> 16) & 0xFF) * percentage2 /
					100 + green1;
				blue = ((buf2[i] >> 8) & 0xFF) * percentage2 /
					100 + blue1;
				outbuf[i] = (red << 24) | (green << 16) |
					(blue << 8) | 255;
			}
			break;
	}
}
