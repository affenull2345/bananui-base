/*
	Graphics library to handle non-qualcomm linux framebuffers.
	Color Format: RGBA 8888 or RGB 565
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _GR_FBDEV_H_
#define _GR_FBDEV_H_
#include <linux/fb.h>
#include <stdint.h>

struct fbdev_fbinfo {
	unsigned char *framebuffer, *real_framebuffer;
	struct fb_fix_screeninfo *finfo;
	struct fb_var_screeninfo *vinfo;
	int fd, ttyfd;
	size_t memlen;
};

void fbdev_fbcpy(struct fbdev_fbinfo *dst, const uint32_t *src, int yoffset,
	int xoffset, int yres, int xres);
int fbdev_getxres(struct fbdev_fbinfo *info);
int fbdev_getyres(struct fbdev_fbinfo *info);
void fbdev_fbcleanup(struct fbdev_fbinfo *info);
void fbdev_refreshScreen(struct fbdev_fbinfo *info);
void fbdev_blankScreen(struct fbdev_fbinfo *info, int blank);
void fbdev_setPixel(struct fbdev_fbinfo *info, int y, int x, int r, int g,
	int b, int a);
void fbdev_drawRect(struct fbdev_fbinfo *info, int y, int x, int h, int w,
	int r, int g, int b, int a);
struct fbdev_fbinfo *fbdev_getfb();

#endif
