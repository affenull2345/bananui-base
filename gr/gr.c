/*
	Forward framebuffer requests to the backend.
	Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdio.h>
#include "gr.h"
#ifdef GR_QCOM
#include "gr_qcom.h"
#endif
#ifdef GR_FBDEV
#include "gr_fbdev.h"
#endif

static void swapEmulatedSavedBackends(struct fbinfo *info)
{
	enum gr_backend tmp;
	tmp = info->backend;
	info->backend = info->emulated_info->saved_backend;
	info->emulated_info->saved_backend = tmp;
}

static void emulated_setPixel(struct emulated_fbinfo *info, int y, int x, int r,
	int g, int b, int a)
{
	if(y >= info->yres || x >= info->xres || y < 0 || x < 0) return;
	if(a != 255){
		uint32_t oldpixel;
		int oldr, oldg, oldb;
		oldpixel = info->buffer[y * info->xres + x];
		oldr = oldpixel >> 24;
		oldg = (oldpixel >> 16) & 0xFF;
		oldb = (oldpixel >> 8) & 0xFF;
		r = (r * a / 255) + (oldr * (255-a) / 255);
		g = (g * a / 255) + (oldg * (255-a) / 255);
		b = (b * a / 255) + (oldb * (255-a) / 255);
	}
	info->buffer[y * info->xres + x] = r << 24 | g << 16 | b << 8 | 255;
}
static void emulated_drawRect(struct emulated_fbinfo *info, int y, int x,
	int h, int w, int r, int g, int b, int a)
{
	int i, j;
	for(i = y; i < y + h; i++){
		if(i + h < 0) break;
		if(i > 0 && i >= info->yres) break;
		for(j = x; j < x + w; j++){
			if(j + w < 0) break;
			if(j > 0 && j >= info->xres) break;
			emulated_setPixel(info, i, j, r, g, b, a);
		}
	}
}
static void emulated_fbcpy(struct emulated_fbinfo *dst, const uint32_t *src,
	int yoffset, int xoffset, int yres, int xres)
{
	int i, j;
	for(i = 0; i < yres && (i+yoffset) < dst->yres; i++){
		if(i+yoffset < 0) continue;
		for(j = 0; j < xres && (j+xoffset) < dst->xres; j++){
			if(j+xoffset < 0) continue;
			dst->buffer[(yoffset+i) * dst->xres + xoffset + j] =
				src[xres * i + j];
		}
	}
}

int getxres(struct fbinfo *info)
{
	switch(info->backend){
		case GR_EMULATED_BACKEND:
			return info->emulated_info->xres;
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			return qcom_getxres(info->qcom_info);
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			return fbdev_getxres(info->fbdev_info);
#endif
		default:
			return 240;
	}
	return 240;
}
int getyres(struct fbinfo *info)
{
	switch(info->backend){
		case GR_EMULATED_BACKEND:
			return info->emulated_info->yres;
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			return qcom_getyres(info->qcom_info);
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			return fbdev_getyres(info->fbdev_info);
#endif
		default:
			return 320;
	}
	return 320;
}
void fbcleanup(struct fbinfo *info)
{
	switch(info->backend){
		case GR_EMULATED_BACKEND:
			if(info->emulated_info->saved_backend !=
				GR_EMULATED_BACKEND)
			{
				swapEmulatedSavedBackends(info);
				fbcleanup(info);
			}
			return;
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			qcom_fbcleanup(info->qcom_info);
			break;
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			fbdev_fbcleanup(info->fbdev_info);
			break;
#endif
		default:
			break;
	}
	free(info);
}
void emulateFramebuffer(struct fbinfo *info, uint32_t **buffer, int yres,
	int xres)
{
	void *tmp;
	tmp = *buffer;
	*buffer = info->generic_info;
	info->emulated_info =
		malloc(sizeof(struct emulated_fbinfo));
	info->emulated_info->yres = yres;
	info->emulated_info->xres = xres;
	info->emulated_info->buffer = tmp;
	info->emulated_info->saved_backend = info->backend;
	info->backend = GR_EMULATED_BACKEND;
}
void restoreFramebuffer(struct fbinfo *info, uint32_t **buffer)
{
	void *tmp;
	if(info->backend != GR_EMULATED_BACKEND) return;
	tmp = *buffer;
	*buffer = info->emulated_info->buffer;
	info->backend = info->emulated_info->saved_backend;
	free(info->emulated_info);
	info->generic_info = tmp;
}
void blankScreen(struct fbinfo *info, int blank)
{
	switch(info->backend){
		case GR_EMULATED_BACKEND:
			if(info->emulated_info->saved_backend !=
				GR_EMULATED_BACKEND)
			{
				swapEmulatedSavedBackends(info);
				blankScreen(info, blank);
				swapEmulatedSavedBackends(info);
			}
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			qcom_blankScreen(info->qcom_info, blank);
			return;
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			fbdev_blankScreen(info->fbdev_info, blank);
			return;
#endif
		default:
			return;
	}
}
void fbcpy(struct fbinfo *dst, const uint32_t *src, int yoffset, int xoffset,
	int yres, int xres)
{
	switch(dst->backend){
		case GR_EMULATED_BACKEND:
			emulated_fbcpy(dst->emulated_info, src, yoffset,
				xoffset, yres, xres);
			return;
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			qcom_fbcpy(dst->qcom_info, src, yoffset, xoffset,
				yres, xres);
			return;
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			fbdev_fbcpy(dst->fbdev_info, src, yoffset, xoffset,
				yres, xres);
			return;
#endif
		default:
			return;
	}
}
void refreshScreen(struct fbinfo *info)
{
	switch(info->backend){
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			qcom_refreshScreen(info->qcom_info);
			return;
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			fbdev_refreshScreen(info->fbdev_info);
			return;
#endif
		default:
			return;
	}
}
void setPixel(struct fbinfo *info, int y, int x, int r, int g, int b, int a)
{
	switch(info->backend){
		case GR_EMULATED_BACKEND:
			emulated_setPixel(info->emulated_info, y, x, r, g, b,
				a);
			return;
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			qcom_setPixel(info->qcom_info, y, x, r, g, b, a);
			return;
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			fbdev_setPixel(info->fbdev_info, y, x, r, g, b, a);
			return;
#endif
		default:
			return;
	}
}
void drawRect(struct fbinfo *info, int y, int x, int h, int w, int r, int g,
	int b, int a)
{
	switch(info->backend){
		case GR_EMULATED_BACKEND:
			emulated_drawRect(info->emulated_info, y, x, h, w, r,
				g, b, a);
			return;
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			qcom_drawRect(info->qcom_info, y, x, h, w, r, g, b,
				a);
			return;
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			fbdev_drawRect(info->fbdev_info, y, x, h, w, r, g, b,
				a);
			return;
#endif
		default:
			return;
	}
}
struct fbinfo *getfb(enum gr_backend betype)
{
	struct fbinfo *info;
	info = malloc(sizeof(struct fbinfo));
	info->backend = betype;
	switch(betype){
		case GR_EMULATED_BACKEND:
			fprintf(stderr, "Error: GR_EMULATED_BACKEND may not be initialized directly\n");
#ifdef GR_QCOM
		case GR_QCOM_BACKEND:
			info->qcom_info = qcom_getfb();
			if(info->qcom_info == NULL){
				free(info);
				return NULL;
			}
			return info;
#endif
#ifdef GR_FBDEV
		case GR_FBDEV_BACKEND:
			info->fbdev_info = fbdev_getfb();
			if(info->fbdev_info == NULL){
				free(info);
				return NULL;
			}
			return info;
#endif
		default:
			return info;
	}
}
