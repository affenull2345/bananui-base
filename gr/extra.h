/*
	Graphics for the Bananui user interface - buffer cross-fade
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _GR_EXTRA_H_
#define _GR_EXTRA_H_

#include <stdint.h>

enum blend_style {
	BLEND_STYLE_CROSSFADE
};

void blend_buffers(uint32_t *buf1, uint32_t *buf2, enum blend_style style,
	int yres, int xres, int percentage1, uint32_t *outbuf);

#endif
