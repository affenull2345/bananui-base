/*
	Graphics library to handle non-qualcomm linux framebuffers.
	Color Format: RGBA 8888 or RGB 565
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <linux/fb.h>
#include <linux/vt.h>
#include <linux/kd.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gr_fbdev.h"
#define FBDEV "/dev/fb0"
#define TTYDEV "/dev/tty"
#ifndef KDSKBMUTE
#define KDSKBMUTE 0x4B51
#endif


void fbdev_fbcleanup(struct fbdev_fbinfo *info)
{
	munmap(info->framebuffer, info->memlen);
	if(info->ttyfd >= 0){
		ioctl(info->ttyfd, KDSETMODE, KD_TEXT);
		ioctl(info->ttyfd, KDSKBMUTE, 0);
		close(info->ttyfd);
	}
	close(info->fd);
	free(info->finfo);
	free(info->vinfo);
	free(info);
}
void fbdev_blankScreen(struct fbdev_fbinfo *info, int blank)
{
	if(blank){
		if(ioctl(info->fd, FBIOBLANK, FB_BLANK_POWERDOWN)){
			perror("ioctl FBIOBLANK FB_BLANK_POWERDOWN");
		}
	}
	else {
		if(ioctl(info->fd, FBIOBLANK, FB_BLANK_UNBLANK)){
			perror("ioctl FBIOBLANK FB_BLANK_UNBLANK");
		}
		fbdev_refreshScreen(info);
	}
}
void fbdev_refreshScreen(struct fbdev_fbinfo *info)
{
	/*if(info->vinfo->yoffset > 0){
		info->vinfo->yoffset = 0;
		memcpy(info->framebuffer + (info->memlen / 2),
			info->framebuffer, info->memlen / 2);
		info->framebuffer += (info->memlen / 2);
	}
	else {
		info->vinfo->yoffset = info->vinfo->yres;
		memcpy(info->framebuffer - (info->memlen / 2),
			info->framebuffer, info->memlen / 2);
		info->framebuffer -= (info->memlen / 2);
	}*/
	memcpy(info->real_framebuffer, info->framebuffer, info->vinfo->yres *
		info->finfo->line_length);
	/*info->vinfo->activate = FB_ACTIVATE_VBL;
	if(ioctl(info->fd, FBIOPAN_DISPLAY, info->vinfo)){
		perror("ioctl FBIOPAN_DISPLAY");
	}*/
}
void fbdev_setPixel(struct fbdev_fbinfo *info, int y, int x, int r, int g,
	int b, int a)
{
	unsigned char *fb;
	if(y > info->vinfo->yres || x > info->vinfo->xres) return;
	fb = info->framebuffer;
	if(a != 255){
		int oldr, oldg, oldb;
		if(info->vinfo->bits_per_pixel == 16){
			uint16_t oldpixel;
			oldpixel = ((uint16_t*)fb)
				[y*info->finfo->line_length/2 + x];
			oldr = (oldpixel >> info->vinfo->red.offset) & 31;
			oldg = (oldpixel >> info->vinfo->green.offset) & 63;
			oldb = (oldpixel >> info->vinfo->blue.offset) & 31;
		}
		else {
			uint32_t oldpixel;
			oldpixel = ((uint32_t*)fb)
				[y*info->finfo->line_length/4 + x];
			oldr = (oldpixel >> info->vinfo->red.offset) & 255;
			oldg = (oldpixel >> info->vinfo->green.offset) & 255;
			oldb = (oldpixel >> info->vinfo->blue.offset) & 255;
		}
		r = (r * a / 255) + (oldr * (255-a) / 255);
		g = (g * a / 255) + (oldg * (255-a) / 255);
		b = (b * a / 255) + (oldb * (255-a) / 255);
	}
	if(info->vinfo->bits_per_pixel == 16){
		r >>= 3;
		g >>= 2;
		b >>= 3;
		((uint16_t*)fb)[y*info->finfo->line_length/2 + x] =
			((r & 31) << info->vinfo->red.offset) |
			((g & 63) << info->vinfo->green.offset) |
			((b & 31) << info->vinfo->blue.offset);
	}
	else {
		((uint32_t*)fb)[y*info->finfo->line_length/4 + x] =
			((r & 255) << info->vinfo->red.offset) |
			((g & 255) << info->vinfo->green.offset) |
			((b & 255) << info->vinfo->blue.offset) |
			((255 >> (8-info->vinfo->transp.length)) <<
				info->vinfo->transp.offset);
	}
}

void fbdev_drawRect(struct fbdev_fbinfo *info, int y, int x, int h, int w,
	int r, int g, int b, int a)
{
	int i, j;
	for(i = y; i < y + h; i++){
		if(i + h < 0) break;
		if(i > 0 && i >= info->vinfo->yres) break;
		for(j = x; j < x + w; j++){
			if(j + w < 0) break;
			if(j > 0 && j >= info->vinfo->xres) break;
			fbdev_setPixel(info, i, j, r, g, b, a);
		}
	}
}
void fbdev_fbcpy(struct fbdev_fbinfo *dst, const uint32_t *src, int yoffset,
	int xoffset, int yres, int xres)
{
	int i, j;
	for(i = 0; i < yres; i++){
		for(j = 0; j < xres; j++){
			uint32_t pix8888 = src[xres * i + j];
			fbdev_setPixel(dst, yoffset+i, xoffset+j,
				(pix8888 >> 24),
				(pix8888 >> 16) & 0xFF,
				(pix8888 >> 8) & 0xFF,
				pix8888 & 0xFF);
		}
	}
}
int fbdev_getxres(struct fbdev_fbinfo *info){
	return info->vinfo->xres;
}
int fbdev_getyres(struct fbdev_fbinfo *info){
	return info->vinfo->yres;
}
struct fbdev_fbinfo *fbdev_getfb(){
	int fd;
	size_t pagesize;
	unsigned char *framebuffer = (unsigned char*)-1;
	struct fb_fix_screeninfo *finfo;
	struct fb_var_screeninfo *vinfo;
	struct fbdev_fbinfo *info;
	finfo = malloc(sizeof(struct fb_fix_screeninfo));
	vinfo = malloc(sizeof(struct fb_var_screeninfo));
	info = malloc(sizeof(struct fbdev_fbinfo));
	info->ttyfd = open(TTYDEV, O_RDONLY);
	if(info->ttyfd < 0){
		perror(TTYDEV);
	}
	else {
		ioctl(info->ttyfd, KDSETMODE, KD_GRAPHICS);
		ioctl(info->ttyfd, KDSKBMUTE, 1);
	}
	fd = open(FBDEV, O_RDWR);
	if(fd < 0){
		perror(FBDEV);
		return NULL;
	}
	if(ioctl(fd, FBIOGET_FSCREENINFO, finfo) < 0){
		perror("ioctl FBIOGET_FSCREENINFO");
		return NULL;
	}
	if(ioctl(fd, FBIOGET_VSCREENINFO, vinfo) < 0){
		perror("ioctl FBIOGET_VSCREENINFO");
		return NULL;
	}
	printf("type: 0x%x\n", finfo->type);
	printf("visual: %d\n", finfo->visual);
	printf("line_length: %d\n", finfo->line_length);
	printf("bits_per_pixel: %d\n", vinfo->bits_per_pixel);
	printf("%dx%d\n", vinfo->xres, vinfo->yres);
	pagesize = sysconf(_SC_PAGESIZE);
	info->memlen = (vinfo->yres_virtual * finfo->line_length +
		(pagesize-1)) & ~(pagesize-1);
	framebuffer = mmap(0, info->memlen,
		PROT_READ | PROT_WRITE,
		MAP_SHARED, fd, 0);
	if(framebuffer == (unsigned char*)-1){
		perror("mmap");
		return NULL;
	}
	if(vinfo->bits_per_pixel == 32){
		if(vinfo->red.length != 8 ||
			vinfo->green.length != 8 ||
			vinfo->blue.length != 8)
		{
			fprintf(stderr, "32-bit framebuffer pixel size mismatch\n");
			return NULL;
		}
	}
	else {
		if(vinfo->red.length != 5 ||
			vinfo->green.length != 6 ||
			vinfo->blue.length != 5 ||
			vinfo->transp.length != 0)
		{
			fprintf(stderr, "16-bit framebuffer pixel size mismatch\n");
			return NULL;
		}
	}
	memset(framebuffer, 0, info->memlen);
	info->finfo = finfo;
	info->vinfo = vinfo;
	info->real_framebuffer = framebuffer;
	info->framebuffer = malloc(info->memlen);
	info->fd = fd;
	return info;
}
