/*
	BananUI font implementation - uses a monospace font
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <string.h>
#include <stdlib.h>
#include "gr/gr.h"
#include "font_10x18.h"

static unsigned char *decompr_fnt;

void font_init(){
	unsigned char compr_byte, *in, *out;
	in = fnt.rundata;
	decompr_fnt = malloc(fnt.height * fnt.width);
	out = decompr_fnt;
	while((compr_byte = *in++)){
		memset(out, compr_byte & 0x80, compr_byte & 0x7f);
		out += compr_byte & 0x7f;
	}
}

int font_get_char_width(char ch)
{
	if(ch >= 0x20 && ch <= 0x7F) return fnt.cwidth;
	else return 0;
}

int font_get_char_height(char ch)
{
	if(ch >= 0x20 && ch <= 0x7F) return fnt.cheight;
	else return 0;
}
int font_get_default_min_line_height()
{
	return fnt.cheight;
}
int font_get_str_width(const char *str)
{
	int w = 0;
	char ch;
	while((ch = *str++)) w += font_get_char_width(ch);
	return w;
}
int font_get_str_height(const char *str)
{
	int h, newh;
	char ch;
	h = font_get_default_min_line_height(str);
	while((ch = *str++)){
		newh = font_get_char_height(ch);
		if(newh > h) h = newh;
	}
	return h;
}

void font_draw_character(struct fbinfo *fbinf, int x, int y, char ch,
	int r, int g, int b, int a)
{
	int offset, i, j;
	if(ch < 0x20 || ch > 0x7F) return;
	offset = (ch - ' ') * (fnt.cwidth);
	for(i = 0; i < fnt.cheight; i++){
		for(j = 0; j < fnt.cwidth; j++){
			if(decompr_fnt[(j + offset)])
			{
				setPixel(fbinf,
					y+i, j+x, r, g, b, a);
			}
		}
		offset += fnt.width;
	}
}
