/*
	BananUI font implementation (header file)
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _FONT_H_
#define _FONT_H_
void font_init();

int font_get_char_width(char ch);
int font_get_char_height(char ch);
int font_get_default_min_line_height();
int font_get_str_width(const char *str);
int font_get_str_height(const char *str);
void font_draw_character(struct fbinfo *fbinf, int x, int y, char ch,
	int r, int g, int b, int a);

#endif
