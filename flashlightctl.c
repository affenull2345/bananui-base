/*
	Simple flashlight control app
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#define FLASHLIGHT_FILE "/sys/class/leds/flashlight/brightness"

#define MAX_RECV_LINE_SIZE 1024
#ifdef HAVE_DEBUG
# define DEBUG(s) printf("DEBUG: %s:%d (in %s): %s\n", __FILE__, __LINE__, \
	__FUNCTION__, s)
#else
# define DEBUG(s)
#endif

static const char string [] =
	"tit Flashlight\n"
	"lbc Flashlight\n"	/* 0 */
	"lbl \n"		/* 1 */
	"lbc OFF\n"		/* 2 */
	"lbl \n"		/* 3 */
	"btn Turn ON\n"		/* 4 */
	"rfr \n";

static const char switchon [] =
	"rep 2 lbc ON\n"
	"rep 4 btn Turn OFF\n"
	"fla 1\n"
	"rfr \n";

static const char switchoff [] =
	"rep 2 lbc OFF\n"
	"rep 4 btn Turn ON\n"
	"fla 0\n"
	"rfr \n";

void updateLight(int fd, int status)
{
	if(status){
		write(fd, switchon, sizeof(switchoff)-1);
	}
	else {
		write(fd, switchoff, sizeof(switchon)-1);
	}
}

int readStatus()
{
	int ledfd, status;
	char ch;
	ledfd = open(FLASHLIGHT_FILE, O_RDONLY);
	read(ledfd, &ch, 1);
	if(ch == '0') status = 0;
	else status = 1;
	close(ledfd);
	return status;
}

void processResponse(int fd, const char *response)
{
	DEBUG(response);
	if(0 == strncmp(response, "clk ", 4)){
		int status;
		status = readStatus();
		updateLight(fd, !status);
	}
	else if(0 == strncmp(response, "exi ", 4)){
		exit(0);
	}
}

int main(){
	struct sockaddr_un addr = {AF_UNIX, "/tmp/bananui.sock"};
	int fd, bufindex = 0;
	char recvbuf[MAX_RECV_LINE_SIZE];
	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(fd < 0){
		perror("socket");
		return 1;
	}
	if(connect(fd, (struct sockaddr*) &addr, sizeof(addr)) < 0){
		perror("Failed to bind to /tmp/bananui.sock");
		return 1;
	}
	if(write(fd, string, sizeof(string)-1) < 0){
		perror("write");
		return 1;
	}
	updateLight(fd, readStatus());
	while(read(fd, recvbuf+bufindex, 1) > 0){
		if(recvbuf[bufindex] == '\n'){
			recvbuf[bufindex] = 0;
			bufindex = 0;
			processResponse(fd, recvbuf);
		}
		else if(bufindex == MAX_RECV_LINE_SIZE - 1){
			char ch;
			recvbuf[bufindex] = 0;
			while(read(fd, &ch, 1) && ch != '\n')
				;
		}
		else bufindex++;
	}
	return 0;
}
