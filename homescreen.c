/*
	App list app (homescreen)
        Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <linux/input-event-codes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <bananui/bananui.h>

#include "ini.h"

#define NUMBER_SIZE 256 /* We don't support longer phone numbers */

enum appstate {
	APP_HOME, APP_APPLIST, APP_CONFIRM_LOGOUT, APP_OPTIONS, APP_DIALER,
	APP_CALLERROR
};

struct appinfo {
	char *icon;
	char *name;
	char *exec;
	int supported, hidden, nodisplay;
	bWidget btn;
};

struct homescreen_data {
	enum appstate state;
	struct appinfo *apps;
	char **datadirs;
	size_t numdatadirs, numapps;
	unsigned int displayhidden	: 1;
	unsigned int displayunsupported	: 1;
	int numberindex;
	char number[NUMBER_SIZE];
	bWidget optRefresh;
	bWidget optToggleShowUnsupported;
	bWidget optToggleShowHidden;
	bWidget numlbl;
};

int desktopFileHandler(void *userdata, const char *section, const char *name,
	const char *value)
{
	struct appinfo *app = userdata;
	if(strcmp(section, "MIME Cache") == 0){
		/* This is not a desktop file - hide it forever */
		app->hidden = 1;
		return 0;
	}
	if(strcmp(section, "Desktop Entry") != 0) return 0;

	if(strcmp(name, "Name") == 0) app->name = strdup(value);
	else if(strcmp(name, "Icon") == 0) app->icon = strdup(value);
	else if(strcmp(name, "Exec") == 0) app->exec = strdup(value);
	else if(strcmp(name, "Hidden") == 0){
		if(strcmp(value, "true") == 0) app->hidden = 1;
	}
	else if(strcmp(name, "NoDisplay") == 0){
		if(strcmp(value, "true") == 0) app->nodisplay = 1;
	}
	else if(strcmp(name, "X-Bananui-Supported") == 0){
		if(strcmp(value, "true") == 0) app->supported = 1;
	}
	return 0;
}

void readApps(struct homescreen_data *data)
{
	int i, j, appi;
	DIR **appdirs;
	if(!data->datadirs){
		int dsti;
		char *datadirs_env;
		datadirs_env = getenv("XDG_DATA_DIRS");
		if(datadirs_env == NULL){
			data->datadirs = malloc(2 * sizeof(char *));
			data->datadirs[0] = "/usr/share";
			data->datadirs[1] = "/usr/local/share";
			data->numdatadirs = 2;
		}
		else {
			data->numdatadirs = 1;
			for(i = 0; datadirs_env[i]; i++){
				if(datadirs_env[i] == ':') data->numdatadirs++;
			}
			data->datadirs = malloc(data->numdatadirs *
				sizeof(char *));
			for(j = 0, dsti = 0, i = 0; datadirs_env[i]; i++){
				if(dsti == 0){
					data->datadirs[j] = malloc(PATH_MAX *
						sizeof(char));
				}
				if(datadirs_env[i] == ':'){
					data->datadirs[j][dsti] = '\0';
					dsti = 0;
					j++;
					continue;
				}
				if(dsti == PATH_MAX-1){
					/* Ignore all source characters */
					while(datadirs_env[i+1] &&
						datadirs_env[i+1] != ':')
					{
						i++;
					}
					continue;
				}
				data->datadirs[j][dsti] = datadirs_env[i];
				dsti++;
			}
			data->datadirs[j][dsti] = '\0';
		}
		for(i = 0; i < data->numdatadirs; i++){
			fprintf(stderr, "Data directory: %s\n",
				data->datadirs[i]);
		}
	}
	appdirs = malloc(data->numdatadirs * sizeof(DIR*));
	for(data->numapps = i = 0; i < data->numdatadirs; i++){
		char appsdirpath[PATH_MAX];
		int dirlen = 0;
		struct dirent *de;
		snprintf(appsdirpath, PATH_MAX, "%s/applications",
			data->datadirs[i]);
		appdirs[i] = opendir(appsdirpath);
		if(!appdirs[i]) continue;
		while((de = readdir(appdirs[i]))){
			int fd;
			struct stat sb;
			fd = openat(dirfd(appdirs[i]), de->d_name,
				O_RDONLY|O_CLOEXEC);
			if(fstat(fd, &sb) || ((sb.st_mode & S_IFMT) != S_IFREG))
				continue;
			dirlen++, data->numapps++;
			close(fd);
		}
		rewinddir(appdirs[i]);
	}
	data->apps = malloc(data->numapps * sizeof(struct appinfo));
	for(appi = i = 0; i < data->numdatadirs; i++){
		struct dirent *de;
		if(!appdirs[i]) continue;
		for(; (de = readdir(appdirs[i])); appi++){
			int fd;
			FILE *file;
			struct stat sb;
			fd = openat(dirfd(appdirs[i]), de->d_name,
				O_RDONLY|O_CLOEXEC);
			if(fstat(fd, &sb) || ((sb.st_mode & S_IFMT) == S_IFDIR))
			{
				appi--;
				continue;
			}
			file = fdopen(fd, "r");
			data->apps[appi].supported = 0;
			data->apps[appi].hidden = 0;
			data->apps[appi].nodisplay = 0;
			data->apps[appi].name = strdup("??? Unnamed app");
			data->apps[appi].icon = strdup("image-missing");
			data->apps[appi].exec = strdup("/bin/echo");
			data->apps[appi].btn = NO_WIDGET;
			ini_parse_file(
				file,
				desktopFileHandler,
				&(data->apps[appi])
			);
			fclose(file);
		}
	}
}

void showHomescreen(bWindow *wnd)
{
	struct homescreen_data *data;
	bColor fgcolor, bgcolor;
	data = bGetWindowData(wnd);

	data->state = APP_HOME;
	bClearWnd(wnd);
	bRGB(&bgcolor, 255, 255, 255);
	bRGB(&fgcolor, 0, 0, 0);
	bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "color", &fgcolor);
	bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "background", &bgcolor);
	bCreateLabel(wnd, "\n\nWelcome to Bananian!", TA_CENTER, NO_WIDGET);
	bSetSoftkeys(wnd, "Log out", "APPS", "");
	bRefreshWnd(wnd, 1);
}

void showApplist(bWindow *wnd)
{
	struct homescreen_data *data;
	int i;
	bColor bgcolor, fgcolor, focolor, bocolor;
	data = bGetWindowData(wnd);

	data->state = APP_APPLIST;
	bClearWnd(wnd);
	bRGB(&bgcolor, 64, 64, 64);
	bRGB(&fgcolor, 255, 255, 255);
	bRGB(&focolor, 64, 64, 255);
	bRGB(&bocolor, 64, 64, 64);
	bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "color", &fgcolor);
	bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "background", &bgcolor);
	bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "border", &bocolor);
	bSetColor(wnd, STYLE_TARGET_FOCUS, "border", &focolor);
	bSetSoftkeys(wnd, "", "OPEN", "Options");
	if(!data->apps) readApps(data);
	for(i = 0; i < data->numapps; i++){
		char *content;
		size_t contentsize;
		if(data->apps[i].hidden)
			continue;
		if(data->apps[i].nodisplay && !data->displayhidden)
			continue;
		if(!data->apps[i].supported && !data->displayunsupported)
			continue;
		/* @[icon]@[name] */
		contentsize = strlen(data->apps[i].icon) +
			strlen(data->apps[i].name) + 3;
		content = malloc(contentsize);
		snprintf(content, contentsize, "@%s@%s", data->apps[i].icon,
			data->apps[i].name);
		data->apps[i].btn = bCreateButton(wnd, content, NO_WIDGET);
		free(content);
	}
	bRefreshWnd(wnd, 1);
}

void confirmLogout(bWindow *wnd)
{
	struct homescreen_data *data;
	data = bGetWindowData(wnd);

	data->state = APP_CONFIRM_LOGOUT;
	bClearWnd(wnd);
	bSetSoftkeys(wnd, "No", "", "Yes");
	bCreateLabel(wnd, "Really log out?", TA_LEFT, NO_WIDGET);
	bRefreshWnd(wnd, 0);
}

void showOptions(bWindow *wnd)
{
	struct homescreen_data *data;
	data = bGetWindowData(wnd);

	data->state = APP_OPTIONS;
	bClearWnd(wnd);
	data->optRefresh = bCreateButton(wnd,
		"@view-refresh@Refresh list", NO_WIDGET);
	data->optToggleShowHidden = bCreateButton(wnd,
		data->displayhidden ?
			"@list-remove@Hide hidden" :
			"@list-add@Show hidden", NO_WIDGET);
	data->optToggleShowUnsupported = bCreateButton(wnd,
		data->displayunsupported ?
			"@list-remove@Hide unsupported" :
			"@list-add@Show unsupported", NO_WIDGET);
	bSetSoftkeys(wnd, "", "SELECT", "");
	bRefreshWnd(wnd, 1);
}

void execute(struct appinfo *app)
{
	int len, percent = 0;
	char *tmp, *tmp2, *out;
	len = strlen(app->exec);
	for(tmp = app->exec; *tmp; tmp++){
		if(!percent && *tmp == '%'){
			percent = 1;
		}
		else if(percent){
			percent = 0;
			switch(*tmp){
				case 'i':
					len += strlen(app->icon) +
						7 /* strlen("--icon ") */;
					break;
				case 'c':
					len += strlen(app->name);
					break;
				case '%':
					len++;
					break;
			}
		}
		else len++;
	}
	out = malloc((len+1) * sizeof(char));
	percent = 0;
	for(tmp = app->exec, tmp2 = out; *tmp; tmp++){
		if(!percent && *tmp == '%'){
			percent = 1;
		}
		else if(percent){
			percent = 0;
			switch(*tmp){
				case 'i':
					strcpy(tmp2, "--icon ");
					tmp2 += 7; /* strlen("--icon ") */
					strcpy(tmp2, app->icon);
					tmp2 += strlen(app->icon);
					break;
				case 'c':
					strcpy(tmp2, app->name);
					tmp2 += strlen(app->name);
					break;
				case '%':
					*tmp2 = '%';
					tmp2++;
					break;
			}
		}
		else {
			*tmp2 = *tmp;
			tmp2++;
		}
	}
	*tmp2 = '\0';
	if(fork() == 0){
		execl("/bin/sh", "sh", "-c", out, NULL);
		perror("execl /bin/sh");
		exit(3);
	}
	free(out);
}

char numkeyToChar(int key)
{
	switch(key){
		case KEY_0: return '0';
		case KEY_1: return '1';
		case KEY_2: return '2';
		case KEY_3: return '3';
		case KEY_4: return '4';
		case KEY_5: return '5';
		case KEY_6: return '6';
		case KEY_7: return '7';
		case KEY_8: return '8';
		case KEY_9: return '9';
		case KEY_NUMERIC_STAR: return '*';
		case KEY_NUMERIC_POUND: return '#';
		default: return '\0';
	}
}

void updateDialer(bWindow *wnd)
{
	size_t numlen;
	struct homescreen_data *data;
	data = bGetWindowData(wnd);

	numlen = strlen(data->number);
	if(numlen < NUMBER_SIZE-1){
		data->number[numlen] = ' '; /* Add padding */
		data->number[numlen+1] = '\0';
	}
	data->numlbl = bCreateLabel(wnd, data->number, TA_RIGHT,
		data->numlbl);
	data->number[numlen] = '\0';
	bRefreshWnd(wnd, 0);
}

void dialNumber(bWindow *wnd, const char *number)
{
	char errbuf[1024];
	int pipefd[2], piperesult, wstatus;
	pid_t chld;
	struct homescreen_data *data;
	struct sigaction sigact;
	data = bGetWindowData(wnd);

	piperesult = pipe(pipefd);
	if(piperesult < 0) perror("pipe");

	sigemptyset(&sigact.sa_mask);
	sigact.sa_handler = SIG_DFL;
	sigact.sa_flags = 0;
	sigaction(SIGCHLD, &sigact, NULL);

	chld = fork();
	if(chld == 0){
		if(piperesult >= 0){
			close(pipefd[0]);
			close(2);
			dup2(pipefd[1], 2);
			close(pipefd[1]);
		}
		execlp("bananui-dial", "bananui-dial", number, NULL);
		fprintf(stderr,
		"Failed to start bananui-dial. Is bananui-callui installed?\n");
		exit(1);
	}
	if(piperesult >= 0){
		int readchars;
		close(pipefd[1]);
		readchars = read(pipefd[0], errbuf, sizeof(errbuf)-1);
		errbuf[readchars] = '\0';
	}
	if(piperesult < 0){
		showHomescreen(wnd);
		goto end;
	}
	close(pipefd[0]);
	if(waitpid(chld, &wstatus, 0) < 0){
		perror("wait");
		showHomescreen(wnd);
		goto end;
	}
	if(!WIFEXITED(wstatus) || WEXITSTATUS(wstatus) != 0){
		bColor fgcolor, bgcolor;

		data->state = APP_CALLERROR;
		bClearWnd(wnd);
		bRGB(&bgcolor, 255, 255, 0);
		bRGB(&fgcolor, 255, 0, 0);
		bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "color", &fgcolor);
		bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "background",
			&bgcolor);
		bCreateLabel(wnd, "Call failed", TA_CENTER, NO_WIDGET);
		bCreateLabel(wnd, errbuf, TA_LEFT, NO_WIDGET);
		bSetSoftkeys(wnd, "", "OK", "");
		bRefreshWnd(wnd, 0);
		sigemptyset(&sigact.sa_mask);
		sigact.sa_handler = SIG_DFL;
		sigact.sa_flags = 0;
		sigaction(SIGCHLD, &sigact, NULL);
		return;
	}
end:
	sigemptyset(&sigact.sa_mask);
	sigact.sa_handler = SIG_DFL;
	sigact.sa_flags = 0;
	sigaction(SIGCHLD, &sigact, NULL);
	showHomescreen(wnd);
}

void inputNumber(bWindow *wnd, int key)
{
	struct homescreen_data *data;
	data = bGetWindowData(wnd);

	if(data->numberindex == NUMBER_SIZE){
		bVibrate(wnd, 20); /* Vibrate - no more space! */
		return;
	}

	data->number[data->numberindex++] = numkeyToChar(key);
	data->number[data->numberindex] = '\0';
	updateDialer(wnd);
}

void showDialer(bWindow *wnd, int initkey)
{
	struct homescreen_data *data;
	bColor fgcolor, bgcolor;

	data = bGetWindowData(wnd);

	data->state = APP_DIALER;
	data->numberindex = 1;
	data->number[0] = numkeyToChar(initkey);
	data->number[1] = ' ';
	data->number[2] = '\0';
	bClearWnd(wnd);
	bRGB(&bgcolor, 0, 64, 0);
	bRGB(&fgcolor, 255, 255, 255);
	bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "color", &fgcolor);
	bSetColor(wnd, STYLE_TARGET_WINDOW_WIDGET, "background", &bgcolor);
	bCreateLabel(wnd, "\n", TA_LEFT, NO_WIDGET);
	data->numlbl = bCreateLabel(wnd, data->number, TA_RIGHT, NO_WIDGET);
	data->number[1] = '\0';
	bSetSoftkeys(wnd, "", "CALL", "Cancel");
	bRefreshWnd(wnd, 0);
}

int eventCallback(bEventType type, bEventData *evinfo)
{
	struct homescreen_data *data;
	int i;
	data = bGetWindowData(evinfo->window);
	if(data->state == APP_HOME){
		if(type == BEV_KEYDOWN && evinfo->val == KEY_ENTER){
			showApplist(evinfo->window);
		}
		else if(type == BEV_KEYDOWN && evinfo->val == KEY_MENU){
			confirmLogout(evinfo->window);
		}
		else if(type == BEV_KEYDOWN && numkeyToChar(evinfo->val)){
			showDialer(evinfo->window, evinfo->val);
		}
	}
	else if(data->state == APP_CALLERROR){
		if(type == BEV_EXIT ||
			(type == BEV_KEYDOWN && evinfo->val == KEY_ENTER))
		{
			showHomescreen(evinfo->window);
		}
	}
	else if(data->state == APP_DIALER){
		if(type == BEV_EXIT){
			data->numberindex--;
			if(data->numberindex == 0){
				showHomescreen(evinfo->window);
			}
			else {
				memmove(data->number+data->numberindex,
					data->number+data->numberindex+1,
					strlen(data->number+data->numberindex));
				updateDialer(evinfo->window);
			}
		}
		else if(type == BEV_KEYDOWN && (evinfo->val == KEY_SEND ||
			evinfo->val == KEY_ENTER))
		{
			dialNumber(evinfo->window, data->number);
		}
		else if(type == BEV_KEYDOWN && evinfo->val == KEY_BACK)
		{
			showHomescreen(evinfo->window);
		}
		else if(type == BEV_KEYDOWN && numkeyToChar(evinfo->val)){
			inputNumber(evinfo->window, evinfo->val);
		}
	}
	else if(data->state == APP_APPLIST){
		if(type == BEV_EXIT){
			showHomescreen(evinfo->window);
		}
		else if(type == BEV_KEYDOWN && evinfo->val == KEY_BACK){
			showOptions(evinfo->window);
		}
		else if(type == BEV_CLICK){
			for(i = 0; i < data->numapps; i++){
				if(evinfo->widget == data->apps[i].btn){
					execute(&(data->apps[i]));
				}
			}
		}
	}
	else if(data->state == APP_CONFIRM_LOGOUT){
		if(type == BEV_EXIT ||
			(type == BEV_KEYDOWN && evinfo->val == KEY_MENU))
		{
			showHomescreen(evinfo->window);
		}
		else if(type == BEV_KEYDOWN && evinfo->val == KEY_BACK)
			return 0; /* Exit normally */
	}
	else if(data->state == APP_OPTIONS){
		if(type == BEV_CLICK && evinfo->widget == data->optRefresh){
			for(i = 0; i < data->numapps; i++){
				free(data->apps[i].icon);
				free(data->apps[i].exec);
				free(data->apps[i].name);
			}
			free(data->apps);
			data->apps = NULL;
			showApplist(evinfo->window);
		}
		else if(type == BEV_CLICK && evinfo->widget ==
			data->optToggleShowUnsupported)
		{
			data->displayunsupported = !data->displayunsupported;
			showApplist(evinfo->window);
		}
		else if(type == BEV_CLICK && evinfo->widget ==
			data->optToggleShowHidden)
		{
			data->displayhidden = !data->displayhidden;
			showApplist(evinfo->window);
		}
		else if(type == BEV_EXIT)
		{
			showApplist(evinfo->window);
		}
	}
	return -1; /* Don't exit, keep going */
}

int main()
{
	struct homescreen_data data;
	struct sigaction sigact;
	bWindow *wnd;

	sigemptyset(&sigact.sa_mask);
	sigact.sa_handler = SIG_DFL;
	sigact.sa_flags = SA_NOCLDWAIT;
	sigaction(SIGCHLD, &sigact, NULL);

	data.apps = NULL;
	data.datadirs = NULL;
	data.displayunsupported = 0;
	data.displayhidden = 0;
	data.optToggleShowUnsupported = NO_WIDGET;
	data.optToggleShowHidden = NO_WIDGET;
	wnd = bCreateWnd();
	if(!wnd) return 1;
	bSetWindowTitle(wnd, "@go-home@Home");

	bSetWindowData(wnd, &data);
	showHomescreen(wnd);
	bSetEventCallback(wnd, eventCallback);

	return bEventLoop(wnd);
}
